#!/bin/sh

objects=
for f in $(find src/smilodon -name '*.c' | sort)
do
    headers=
    for l in $(grep '#include "' $f)
    do
        if [ $l = '#include' ]
        then
            continue
        fi
        l=${l#'"'}
        l=${l%'"'}
        if [ $l = 'build_config.h' -o $l = 'html_tags.gperf.h' -o $l = 'html_attrs.gperf.h' ]
        then
            l="\$(BUILDDIR)/generated/$l"
        else
            l=src/$l
        fi
        headers="$headers $l"
    done
    name=${f#src/}
    name=${name%.c}
    echo "c_target(\`\`$name'', \`\`$headers'')"
    objects="$objects object(\`\`$name'')"
done
echo "OBJ =$objects"
