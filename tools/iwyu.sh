#!/bin/sh

# run include-what-you-use on the source files.
# run as: ./iwyu.sh < CMakeLists.txt

DIR="$(dirname "$(realpath $0)")"

mode=n

while true
do
    read line
    if [ "$line" = "set(SOURCES" ]
    then
        mode=y
    elif [ "$line" = ")" ]
    then
        break
    elif [ $mode = y ]
    then
        for file in $line
        do
            iwyu -I "$DIR/src" -I "/usr/include/ClearSilver" \
                 -I "$DIR/build/generated" -D__BEGIN_DECLS= $file 2>&1
        done
    fi
done
