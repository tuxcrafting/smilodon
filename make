#!/bin/sh

name=$(mktemp /tmp/XXXXXX.make)
m4 make.m4 > $name
make -f $name $@ _MAKEFILE=$name
ec=$?
rm $name
exit $ec
