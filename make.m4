.POSIX:

# base definitions
BUILDDIR = build
PREFIX = /usr/local
DESTDIR =
DEBUG = 0
CC = cc

INSTALLDIR = $(DESTDIR)$(PREFIX)

# debug CFLAGS definitions
DEBUG = 0
CFLAGS0 = -O2
CFLAGS1 = -O0 -g

# build CFLAGS
CFLAGS =
CFLAGSbase = -Wall -std=gnu99 -Isrc -I$(BUILDDIR)/generated -D__BEGIN_DECLS=
CFLAGSfinal = $(CFLAGS) $(CFLAGSbase) $(CFLAGS$(DEBUG))

# libraries
LDLIBS = -largon2 -lcrypto -lcurl -lfcgi -ljansson -lneo_cgi -lneo_cs -lneo_utl -lpq -lpthread -lrt -lxml2 -lz

# linker flags
LDFLAGS =
LDFLAGSfinal = $(LDFLAGS) $(LDLIBS)

dnl macros
define(`c_target', `$(BUILDDIR)/obj/$1.o: src/$1.c $2
	@mkdir -p $(shell dirname $`'@)
	$(CC) -c -o $`'@ $< $(CFLAGSfinal)')
define(`object', `$(BUILDDIR)/obj/$1.o')

all: $(BUILDDIR)/smilodon

$(BUILDDIR)/generated/build_config.h: src/build_config.h.in
	@mkdir -p $(shell dirname $`'@)
	sed "s/@GIT_REFSPEC@/$(shell cut -c 6- .git/HEAD | sed 's/\//\\\//g')/" $< | sed "s/@GIT_SHA1@/$(shell git rev-parse HEAD)/" > $@

$(BUILDDIR)/generated/%.gperf.h: src/%.gperf
	@mkdir -p $(shell dirname $`'@)
	gperf $< -lcCEI > $@

define(`fname', maketemp(`/tmp/XXXXXX.m4'))
syscmd(sh tools/objtargets.sh > fname)
include(fname)

$(BUILDDIR)/smilodon: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGSfinal)

clean:
	rm -rf $(BUILDDIR)/obj $(BUILDDIR)/build_config.h $(BUILDDIR)/smilodon $(BUILDDIR)/dist $(BUILDDIR)/smilodon.tar.gz

install: all
	mkdir -p $(INSTALLDIR)/bin
	mkdir -p $(INSTALLDIR)/share/smilodon
	cp -r langs templates static $(INSTALLDIR)/share/smilodon
	cp $(BUILDDIR)/smilodon $(INSTALLDIR)/bin

dist: all
	@mkdir -p $(BUILDDIR)/dist
	$(MAKE) -f $(_MAKEFILE) install DESTDIR=$(BUILDDIR)/dist
	cd $(BUILDDIR)/dist; tar czf ../smilodon.tar.gz *

help:
	@echo 'Configurable macros:'
	@echo '- BUILDDIR = $(BUILDDIR)'
	@echo '  Build directory.'
	@echo '- PREFIX = $(PREFIX)'
	@echo '  Installation prefix.'
	@echo '- DESTDIR = $(DESTDIR)'
	@echo '  Installation root.'
	@echo '- DEBUG = 0'
	@echo '  Debug mode (0 or 1).'
	@echo '- CC = cc'
	@echo '  C compiler.'
	@echo '- CFLAGS ='
	@echo '  C compiler flags.'
	@echo '- LDLIBS = $(LDLIBS)'
	@echo '  Libraries to be linked with.'
	@echo '- LDFLAGS ='
	@echo '  Linker flags.'
	@echo 'Targets:'
	@echo '- all'
	@echo '  Build everything.'
	@echo '- clean'
	@echo '  Remove build files.'
	@echo '- install'
	@echo '  Install at the directory specified by $$(DESTDIR)$$(PREFIX).'
	@echo '- dist'
	@echo '  Create an installation tarball.'
