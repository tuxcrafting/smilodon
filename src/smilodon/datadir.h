/**
 * @file
 * @brief Helpers relating to the data directory.
 */

#pragma once

/**
 * @brief Initialize related structures.
 */
void datadir_init();

/**
 * @brief Get the absolute path of a filename.
 * @param filename Filename, relative to the data directory.
 * @return Dynamically allocated buffer.
 */
char *datadir_path(const char *filename);
