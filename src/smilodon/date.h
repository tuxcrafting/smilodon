/**
 * @file
 * @param Date conversion helpers.
 */

#pragma once

#include <time.h>

/**
 * @brief Convert a timestamp to an RFC 822 string.
 * @param t Timestamp.
 * @param buf Output buffer.
 */
void to_rfc822(time_t t, char *buf);

/**
 * @brief Parse an ISO 8601 timestamp.
 * @param buf Buffer.
 * @return Timestamp.
 */
time_t parse_iso8601(const char *buf);

/**
 * @brief Convert a timestamp to an ISO 8601 string.
 * @param t Timestamp.
 * @param buf Output buffer.
 * @param tz_h Timezone hours.
 * @param tz_m Timezone minutes.
 * @param nice If 1, the date will use spaces to be easier to read; else, normal ISO 8601 will be used.
 */
void to_iso8601(time_t t, char *buf, int tz_h, int tz_m, char nice);
