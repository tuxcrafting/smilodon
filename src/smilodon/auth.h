/**
 * @file
 * @brief Authentication functions.
 *
 * All numbers are big endian.
 * The authentication cookie is a '1', followed by the base64 encoding of:
 * - HMAC_SHA256(user_id[8] || random[8] || password_hash[96]) || user_id[8] || random[8]
 * The random field is the same in both positions it appears.
 */

#pragma once

#include <jansson.h>
#include <util/neo_hdf.h>

/**
 * @brief Try to login.
 * @param username Username.
 * @param password Password.
 * @return User object if successful, NULL otherwise.
 */
json_t *auth_login(const char *username, const char *password);

/**
 * @brief Create an authentication cookie.
 * @param buf Buffer, at least 66 bytes in length.
 * @param obj User object.
 */
void auth_cookie_create(char *buf, json_t *obj);

/**
 * @brief Verify an authentication cookie.
 * @param buf Buffer.
 * @return The actor object, or NULL in case of error.
 */
json_t *auth_cookie_verify(const char *buf);
