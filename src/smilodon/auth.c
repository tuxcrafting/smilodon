#include "smilodon/auth.h"

#include <argon2.h>
#include <inttypes.h>
#include <jansson.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/rand.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "smilodon/config.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"

json_t *auth_login(const char *username, const char *password) {
	// get the user object.
	json_t *obj = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR(username, username)));
	db_check_error(1);

	if (obj == NULL) {
		return NULL;
	}

	// check the password.
	const char *jpass = json_object_string(obj, "password");
	if (argon2id_verify(jpass, password, strlen(password)) != ARGON2_OK) {
		json_decref(obj);
		return NULL;
	}

	return obj;
}

void auth_cookie_create(char *buf, json_t *obj) {
	// get the 64-bit actor ID.
	const char *actor = json_object_string(obj, "actor");
	char *num_id = strrchr(actor, '=') + 1;
	uint64_t id;
	sscanf(num_id, "%" PRIu64, &id);

	// and the password hash.
	const char *pass = json_object_string(obj, "password");

	// make HMAC plaintext.
	// 8 + 8 + 96 (password hashes are always 96 bytes).
	uint8_t plaintext[112];
	// big-endian actor ID.
	plaintext[0] = id >> 56;
	plaintext[1] = (id >> 48) & 0xFF;
	plaintext[2] = (id >> 40) & 0xFF;
	plaintext[3] = (id >> 32) & 0xFF;
	plaintext[4] = (id >> 24) & 0xFF;
	plaintext[5] = (id >> 16) & 0xFF;
	plaintext[6] = (id >> 8) & 0xFF;
	plaintext[7] = id & 0xFF;
	// random data.
	RAND_bytes(plaintext + 8, 8);
	// password hash.
	memcpy(plaintext + 16, pass, 96);

	// HMAC the plaintext.
	uint8_t payload[48];
	HMAC(EVP_sha256(), secret, strlen(secret), plaintext, 112, payload, NULL);
	// copy the user ID and random bytes.
	memcpy(payload + 32, plaintext, 16);

	// base64 encode.
	buf[0] = '1';
	EVP_EncodeBlock((uint8_t*)buf + 1, payload, 48);
}

json_t *auth_cookie_verify(const char *buf) {
	// check the cookie is version 1, the length is valid, and decode the data.
	if (buf[0] != '1' || strlen(buf) != 65) {
		return 0;
	}
	uint8_t payload[48];
	EVP_DecodeBlock(payload, (uint8_t*)buf + 1, 64);

	// extract the user ID.
	uint64_t id =
		((uint64_t)payload[32] << 56) |
		((uint64_t)payload[33] << 48) |
		((uint64_t)payload[34] << 40) |
		((uint64_t)payload[35] << 32) |
		((uint64_t)payload[36] << 24) |
		((uint64_t)payload[37] << 16) |
		((uint64_t)payload[38] << 8) |
		(uint64_t)payload[39];

	// get the user entry.
	char user_id[64];
	snprintf(user_id, 64, "%" PRIu64, id);

	json_t *obj = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR($$meta.id, user_id)));
	db_check_error(1);

	const char *actor_id = json_object_string(obj, "actor");

	if (obj == NULL) {
		return NULL;
	}

	const char *password = json_object_string(obj, "password");

	// create the HMAC plaintext.
	uint8_t plaintext[112];
	memcpy(plaintext, payload + 32, 16);
	memcpy(plaintext + 16, password, 96);

	// check if the HMAC matches.
	uint8_t hmac[32];
	HMAC(EVP_sha256(), secret, strlen(secret), plaintext, 112, hmac, NULL);
	if (memcmp(payload, hmac, 32) != 0) {
		json_decref(obj);
		return NULL;
	}

	json_t *actor = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(id, actor_id)));
	db_check_error(1);

	json_decref(obj);

	return actor;
}
