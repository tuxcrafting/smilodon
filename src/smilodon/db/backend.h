/**
 * @file
 * @brief Common structures to define a backend.
 */

#pragma once

#include <jansson.h>

#include "smilodon/db/database.h"

struct backend_functions {
	_idbc_t *(*dbc_open)(const char*);
	void (*dbc_close)(dbc_t*);
	void *(*trans_commit)(transaction_t*);
	_ids_t *(*ds_open)(dbc_t*, ds_def_t*);
	_icur_t *(*ds_select_ordered)(ds_t*, const char***, const char*, char, int);
	int (*ds_count)(ds_t*, const char***);
	void (*ds_close)(ds_t*);
	int (*cur_nrows)(cur_t*);
	json_t *(*cur_get)(cur_t*, int row);
	void (*cur_close)(cur_t*);
};
