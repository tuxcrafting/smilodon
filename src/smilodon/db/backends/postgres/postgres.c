#include "smilodon/db/backends/postgres/postgres.h"

#include <inttypes.h>
#include <jansson.h>
#include <postgresql/libpq-fe.h>
#include <pthread.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#include "smilodon/db/backend.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"

struct backend_dbc {
	PGconn *conn;
	pthread_mutex_t tlock;
	pthread_mutex_t slock;
};

struct backend_ds {
	char *tbl_name;
};

struct backend_cur {
	json_t **rows;
	int nrows;
};

static PGresult *safe_PQexec(pthread_mutex_t *tlock, PGconn *conn, const char *str) {
	if (tlock != NULL) {
		pthread_mutex_lock(tlock);
	}
	PGresult *res = PQexec(conn, str);
	if (tlock != NULL) {
		pthread_mutex_unlock(tlock);
	}
	return res;
}
static PGresult *safe_PQexecParams(pthread_mutex_t *tlock, PGconn *conn, const char *str,
                                   int nParams, const Oid *types, const char **vals,
                                   const int *lens, const int *formats, int resf) {
	if (tlock != NULL) {
		pthread_mutex_lock(tlock);
	}
	PGresult *res = PQexecParams(conn, str, nParams, types, vals, lens, formats, resf);
	if (tlock != NULL) {
		pthread_mutex_unlock(tlock);
	}
	return res;
}

#define PQexec(conn, str) safe_PQexec(&dbc->dbc->slock, conn, str)
#define PQexecParams(conn, str, nParams, types, vals, lens, formats, resf) \
	safe_PQexecParams(&dbc->dbc->slock, conn, str, nParams, types, vals, lens, formats, resf)

static char _chkret(PGresult *res) {
	if (PQresultStatus(res) != PGRES_COMMAND_OK &&
	    PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_set_error(PQresultErrorMessage(res));
		return 0;
	}
	return 1;
}

// macro that automatically returns NULL after performing
// cleanup actions in case the given PGresult's status is not OK.
#define chkret_r(res, actions, r)	  \
	if (!_chkret(res)) { \
		actions; \
		return r; \
	}
#define chkret(res, actions) chkret_r(res, actions, NULL)

static void *_tbegin(dbc_t *dbc) {
	pthread_mutex_lock(&dbc->dbc->tlock);

	PGresult *res = safe_PQexec(NULL, dbc->dbc->conn, "BEGIN;");
	chkret(res, PQclear(res); pthread_mutex_unlock(&dbc->dbc->tlock));
	PQclear(res);

	return (void*)-1;
}
static void *_tcommit(dbc_t *dbc) {
	PGresult *res = safe_PQexec(NULL, dbc->dbc->conn, "COMMIT;");
	chkret(res, PQclear(res); pthread_mutex_unlock(&dbc->dbc->tlock));
	PQclear(res);

	pthread_mutex_unlock(&dbc->dbc->tlock);

	return (void*)-1;
}
static void *_trollback(dbc_t *dbc) {
	PGresult *res = safe_PQexec(NULL, dbc->dbc->conn, "ROLLBACK;");
	chkret(res, PQclear(res); pthread_mutex_unlock(&dbc->dbc->tlock));
	PQclear(res);

	pthread_mutex_unlock(&dbc->dbc->tlock);

	return (void*)-1;
}

// begin a postgres transaction. this is guarded by a mutex.
#define BEGIN	  \
	if (_tbegin(dbc) == NULL) { \
		return NULL; \
	}

// commit a postgres transaction and unlock the mutex.
#define COMMIT	  \
	if (_tcommit(dbc) == NULL) { \
		return NULL; \
	}

// rollback a postgres transaction and unlock the mutex.
#define ROLLBACK	  \
	if (_trollback(dbc) == NULL) { \
		return NULL; \
	}

// translate a "JSON path" (attribute names separated by a dot)
// to a SQL equivalent on the column "data".
static char *translate_jpath(const char *jpath, char *buf, size_t buf_len) {
	strcpy(buf, "data->'");
	size_t bi = 7;
	for (size_t i = 0; jpath[i] != '\0'; i++) {
		if (bi >= buf_len) {
			return NULL;
		}
		if (jpath[i] == '.') {
			if (bi + 4 >= buf_len) {
				return NULL;
			}
			buf[bi++] = '\'';
			buf[bi++] = '-';
			buf[bi++] = '>';
			buf[bi++] = '\'';
		} else {
			buf[bi++] = jpath[i];
		}
	}
	if (bi >= buf_len) {
		return NULL;
	}
	buf[bi++] = '\'';
	return buf;
}

// translate a condition to a postgresql where clause.
static char translate_cond(const char ***condition, char *buf, size_t buf_len,
                           char **params, size_t params_len, int *pi) {
	strcpy(buf, "('t'");
	json_t *o;
	size_t bi = 4;
	for (size_t i = 0; condition[i] != NULL; i++) {
		if (bi + 5 >= buf_len) {
			return 0;
		}
		strcpy(&buf[bi], ")AND(");
		bi += 5;

		size_t j = 0;
		char invert = 0;
		const char *a = condition[i][j];
		const char *c = condition[i][j + 1];

	translate_cond_s:
		switch ((size_t)c) {
		case (size_t)NOT:
			invert = 1;
			c = condition[i][j + 2];
			j++;
			if (bi + 4 >= buf_len) {
				return 0;
			}
			strcpy(&buf[bi], "NOT(");
			bi += 4;
			goto translate_cond_s;
		case (size_t)OFTYPE:
			if (bi + 13 >= buf_len) {
				return 0;
			}
			strcpy(&buf[bi], "jsonb_typeof(");
			bi += 13;
			if (!translate_jpath(a, &buf[bi], buf_len - bi)) {
				return 0;
			}
			bi = strlen(buf);
			if (bi + 3 >= buf_len) {
				return 0;
			}
			strcpy(&buf[bi], ")='");
			bi += 3;
			switch ((size_t)condition[i][j + 2]) {
			case (size_t)TYPE_STRING:
				if (bi + 6 >= buf_len) {
					return 0;
				}
				strcpy(&buf[bi], "string");
				bi += 6;
				break;
			case (size_t)TYPE_OBJECT:
				if (bi + 6 >= buf_len) {
					return 0;
				}
				strcpy(&buf[bi], "object");
				bi += 6;
				break;
			}
			if (bi + 1 >= buf_len) {
				return 0;
			}
			buf[bi++] = '\'';
			j += 3;
			break;
		case (size_t)EQ_STRING:
			if (!translate_jpath(a, &buf[bi], buf_len - bi)) {
				return 0;
			}
			bi = strlen(buf);
			if (*pi >= params_len) {
				return 0;
			}
			o = json_string(condition[i][j + 2]);
			params[*pi] = json_dumps(o, JSON_ENCODE_ANY);
			json_decref(o);
			if (snprintf(&buf[bi], buf_len - bi, "=$%d::jsonb", (int)++*pi) >= buf_len - bi) {
				return 0;
			}
			bi = strlen(buf);
			j += 3;
			break;
		case (size_t)LT:
			if (!translate_jpath(a, &buf[bi], buf_len - bi)) {
				return 0;
			}
			bi = strlen(buf);
			if (*pi >= params_len) {
				return 0;
			}
			o = json_string(condition[i][j + 2]);
			params[*pi] = json_dumps(o, JSON_ENCODE_ANY);
			json_decref(o);
			if (snprintf(&buf[bi], buf_len - bi, "<$%d::jsonb", (int)++*pi) >= buf_len - bi) {
				return 0;
			}
			bi = strlen(buf);
			j += 3;
			break;
		case (size_t)LE:
			if (!translate_jpath(a, &buf[bi], buf_len - bi)) {
				return 0;
			}
			bi = strlen(buf);
			if (*pi >= params_len) {
				return 0;
			}
			o = json_string(condition[i][j + 2]);
			params[*pi] = json_dumps(o, JSON_ENCODE_ANY);
			json_decref(o);
			if (snprintf(&buf[bi], buf_len - bi, "<=$%d::jsonb", (int)++*pi) >= buf_len - bi) {
				return 0;
			}
			bi = strlen(buf);
			j += 3;
			break;
		}
		if (invert) {
			if (bi + 1 >= buf_len) {
				return 0;
			}
			buf[bi++] = ')';
		}
	}
	if (bi + 1 >= buf_len) {
		return 0;
	}
	buf[bi] = ')';
	return 1;
}

#include "smilodon/db/backends/postgres/dbc.h"
#include "smilodon/db/backends/postgres/trans.h"
#include "smilodon/db/backends/postgres/ds.h"
#include "smilodon/db/backends/postgres/cur.h"

struct backend_functions postgres_functions = {
	.dbc_open = _postgres_dbc_open,
	.dbc_close = _postgres_dbc_close,
	.trans_commit = _postgres_trans_commit,
	.ds_open = _postgres_ds_open,
	.ds_select_ordered = _postgres_ds_select_ordered,
	.ds_count = _postgres_ds_count,
	.ds_close = _postgres_ds_close,
	.cur_nrows = _postgres_cur_nrows,
	.cur_get = _postgres_cur_get,
	.cur_close = _postgres_cur_close,
};
