// cur_ functions.

static int _postgres_cur_nrows(cur_t *cur) {
	db_set_error(NULL);
	return cur->cur->nrows;
}

static json_t *_postgres_cur_get(cur_t *cur, int row) {
	if (row < 0 || row >= cur->cur->nrows) {
		db_set_error("Row number not in range.");
		return NULL;
	}

	json_incref(cur->cur->rows[row]);

	db_set_error(NULL);
	return cur->cur->rows[row];
}

static void _postgres_cur_close(cur_t *cur) {
	for (int i = 0; i < cur->cur->nrows; i++) {
		json_decref(cur->cur->rows[i]);
	}
	sfree(cur->cur->rows);
	sfree(cur->cur);
	db_set_error(NULL);
}
