// trans_ functions.

static void *_postgres_trans_commit(transaction_t *trans) {
	dbc_t *dbc = trans->ds->dbc;

	BEGIN;

	transaction_t *u = trans;
	while (u != NULL) {
		ds_t *ds = u->ds;
		if (u->condition == NULL) {
			// if the condition is null, insert.
			char *s = json_dumps(u->object, 0);

			char *query = salloc(256);
			snprintf(
				query, 256,
				"INSERT"
				" INTO %s"
				"  (data)"
				" VALUES"
				"  ($1)",
				ds->ds->tbl_name);

			PGresult *res = safe_PQexecParams(
				NULL,
				dbc->dbc->conn,
				query,
				1, NULL, (const char**)&s, NULL, NULL, 0);
			chkret(res, PQclear(res); sfree(s); sfree(query); ROLLBACK);
			PQclear(res);

			sfree(query);
			sfree(s);
		} else {
			// else, update.
			char *params[24] = { 0 };
			char *buffer = salloc(1024);
			int pi = 0;

			if (u->object == NULL) {
				snprintf(buffer, 1024,
				         "DELETE"
				         " FROM %s"
				         " WHERE ",
				         ds->ds->tbl_name);
			} else {
				snprintf(buffer, 1024,
				         "UPDATE"
				         "  %s"
				         " SET data = $1"
				         " WHERE ",
				         ds->ds->tbl_name);
				pi++;
				params[0] = json_dumps(u->object, 0);
			}

			size_t base = strlen(buffer);
			translate_cond(u->condition, &buffer[base], 1024 - base, params, 24, &pi);

			PGresult *res = safe_PQexecParams(
				NULL,
				dbc->dbc->conn,
				buffer,
				pi, NULL, (const char**)params, NULL, NULL, 0);
			chkret(res, for (size_t i = 0; i < 24; i++) sfree(params[i]); sfree(buffer); ROLLBACK);
			PQclear(res);

			for (size_t i = 0; i < 24; i++) {
				sfree(params[i]);
			}
			sfree(buffer);
		}

		u = u->cdr;
	}

	COMMIT;
	db_set_error(NULL);
	return (void*)-1;
}
