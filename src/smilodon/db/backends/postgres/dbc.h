// dbc_ functions.

// log postgresql notices to the syslog.
static void notice_processor(void *arg, const char *msg) {
	syslog(LOG_NOTICE, "%s", msg);
}

static _idbc_t *_postgres_dbc_open(const char *conn_string) {
	PGconn *conn = PQconnectdb(conn_string);
	if (conn == NULL || PQstatus(conn) != CONNECTION_OK) {
		db_set_error(PQerrorMessage(conn));
		return NULL;
	}

	PQsetNoticeProcessor(conn, &notice_processor, NULL);

	// only log warnings or up.
	PGresult *res = safe_PQexec(
		NULL,
		conn,
		"SET client_min_messages = warning;");
	chkret(res, PQclear(res); PQfinish(conn));
	PQclear(res);

	// create a table containing a name -> table name mapping of data stores.
	res = safe_PQexec(
		NULL,
		conn,
		"CREATE TABLE IF NOT EXISTS"
		" datastores"
		"("
		" id SERIAL PRIMARY KEY,"
		" name TEXT NOT NULL,"
		" tbl_name TEXT NOT NULL"
		");"
		"CREATE INDEX IF NOT EXISTS"
		"  datastores_name"
		" ON datastores"
		"  (name);");
	chkret(res, PQclear(res); PQfinish(conn));
	PQclear(res);

	// create a table containing a (data store, name) -> index name mapping of indices.
	res = safe_PQexec(
		NULL,
		conn,
		"CREATE TABLE IF NOT EXISTS"
		" indices"
		"("
		" id SERIAL PRIMARY KEY,"
		" datastore TEXT NOT NULL,"
		" name TEXT NOT NULL,"
		" ind_name TEXT NOT NULL"
		");"
		"CREATE INDEX IF NOT EXISTS"
		"  indices_datastore_name"
		" ON indices"
		"  (datastore, name);"
		"CREATE INDEX IF NOT EXISTS"
		"  indices_datastore"
		" ON indices"
		"  (datastore);");
	chkret(res, PQclear(res));
	PQclear(res);

	_idbc_t *dbc = salloc(sizeof(_idbc_t));
	dbc->conn = conn;
	pthread_mutex_init(&dbc->tlock, NULL);
	pthread_mutex_init(&dbc->slock, NULL);

	db_set_error(NULL);
	return dbc;
}

static void _postgres_dbc_close(dbc_t *dbc) {
	PQfinish(dbc->dbc->conn);
	sfree(dbc->dbc);

	db_set_error(NULL);
}
