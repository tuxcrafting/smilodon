// ds_ functions.

static _ids_t *_postgres_ds_open(dbc_t *dbc, ds_def_t *def) {
	const char *params[3];

	PGresult *res = PQexecParams(
		dbc->dbc->conn,
		"SELECT"
		"  tbl_name"
		" FROM datastores"
		" WHERE name = $1;",
		1, NULL, &def->name, NULL, NULL, 0);
	chkret(res, PQclear(res));

	char *tbl_name;

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_set_error(PQresultErrorMessage(res));
		PQclear(res);
		return NULL;
	}

	if (PQntuples(res) == 0) {
		// in case there doesn't exist a table, create it and call _postgres_db_open again.
		PQclear(res);

		tbl_name = salloc(32);
		snprintf(tbl_name, 32, "datastore_%" PRIu64, get_num_id());

		params[0] = def->name;
		params[1] = tbl_name;

		char *query = salloc(128);
		snprintf(
			query, 128,
			"CREATE TABLE"
			" %s"
			"("
			" id SERIAL PRIMARY KEY,"
			" data JSONB"
			");",
			tbl_name);

		BEGIN;

		res = PQexecParams(
			dbc->dbc->conn,
			"INSERT"
			" INTO datastores"
			"  (name, tbl_name)"
			" VALUES"
			"  ($1, $2);",
			2, NULL, params, NULL, NULL, 0);
		chkret(res, PQclear(res); sfree(query); sfree(tbl_name); ROLLBACK);
		PQclear(res);

		res = PQexec(dbc->dbc->conn, query);
		chkret(res, PQclear(res); sfree(query); sfree(tbl_name); ROLLBACK);
		PQclear(res);

		COMMIT;

		sfree(tbl_name);
		sfree(query);

		return _postgres_ds_open(dbc, def);
	} else {
		tbl_name = PQgetvalue(res, 0, 0);
	}

	PGresult *tbl_res = res;

	params[0] = def->name;

	// create non-existent indices.
	for (size_t i = 0; def->indices[i] != NULL; i++) {
		params[1] = def->indices[i];

		res = PQexecParams(
			dbc->dbc->conn,
			"SELECT"
			"  count(*)"
			" FROM indices"
			" WHERE (datastore, name) = ($1, $2);",
			2, NULL, params, NULL, NULL, 0);
		chkret(res, PQclear(res); PQclear(tbl_res));

		if (strcmp(PQgetvalue(res, 0, 0), "0") == 0) {
			PQclear(res);

			char *index = salloc(32);
			snprintf(index, 32, "index_%" PRIu64, get_num_id());
			params[2] = index;

			char *ival = salloc(256);
			if (translate_jpath(def->indices[i], ival, 256) == NULL) {
				sfree(index);
				db_set_error("Index path too long.");
				return NULL;
			}

			char *query = salloc(1024);
			snprintf(
				query, 1024,
				"CREATE INDEX"
				"  %s"
				" ON %s"
				"  ((%s))"
				" WHERE"
				"  length((%s)::text) < 1024;",
				index, tbl_name, ival, ival);

			sfree(ival);

			BEGIN;

			res = PQexecParams(
				dbc->dbc->conn,
				"INSERT"
				" INTO indices"
				"  (datastore, name, ind_name)"
				" VALUES"
				"  ($1, $2, $3);",
				3, NULL, params, NULL, NULL, 0);
			chkret(res, PQclear(res); PQclear(tbl_res); sfree(index); sfree(query));
			PQclear(res);

			res = PQexec(dbc->dbc->conn, query);
			chkret(res, PQclear(res); PQclear(tbl_res); sfree(index); sfree(query));
			PQclear(res);

			COMMIT;

			sfree(index);
			sfree(query);
		} else {
			PQclear(res);
		}
	}

	res = PQexecParams(
		dbc->dbc->conn,
		"SELECT"
		"  name,"
		"  ind_name"
		" FROM indices"
		" WHERE datastore = $1",
		1, NULL, params, NULL, NULL, 0);
	chkret(res, PQclear(res); PQclear(tbl_res));

	PGresult *ind_res = res;

	// delete unspecified indices.
	int num = PQntuples(ind_res);
	for (int i = 0; i < num; i++) {
		const char *name = PQgetvalue(ind_res, i, 0);

		char found = 0;
		for (size_t j = 0; def->indices[j] != NULL; j++) {
			if (strcmp(name, def->indices[j]) == 0) {
				found = 1;
				break;
			}
		}

		if (!found) {
			char *query = salloc(128);
			snprintf(
				query, 128,
				"DROP INDEX %s",
				PQgetvalue(ind_res, i, 1));

			params[1] = name;

			BEGIN;

			res = PQexec(dbc->dbc->conn, query);
			chkret(res, PQclear(res); PQclear(tbl_res); PQclear(ind_res); sfree(query));
			PQclear(res);

			res = PQexecParams(
				dbc->dbc->conn,
				"DELETE"
				" FROM indices"
				" WHERE (datastore, name) = ($1, $2)",
				2, NULL, params, NULL, NULL, 0);
			chkret(res, PQclear(res); PQclear(tbl_res); PQclear(ind_res); sfree(query));
			PQclear(res);

			COMMIT;

			sfree(query);
		}
	}

	_ids_t *ids = salloc(sizeof(_ids_t));
	ids->tbl_name = salloc(strlen(tbl_name) + 1);
	strcpy(ids->tbl_name, tbl_name);

	PQclear(ind_res);
	PQclear(tbl_res);

	db_set_error(NULL);

	return ids;
}

static _icur_t *_postgres_ds_select_ordered(ds_t *ds, const char ***condition,
                                            const char *key, char descending,
                                            int limit) {
	dbc_t *dbc = ds->dbc;

	char *query = salloc(2048);
	char *params[24] = { 0 };

	snprintf(query, 2048,
	         "SELECT"
	         "  data"
	         " FROM %s"
	         " WHERE ",
	         ds->ds->tbl_name);

	int pi = 0;
	size_t len = strlen(query);
	translate_cond(condition, &query[len], 2048 - len, params, 24, &pi);

	if (key != NULL) {
		len = strlen(query);
		snprintf(query + len, 2048 - len,
		         " ORDER BY ");
		len = strlen(query);
		translate_jpath(key, query + len, 2048 - len);
		if (descending) {
			len = strlen(query);
			snprintf(query + len, 2048 - len,
			         " DESC");
		}
	}

	if (limit != -1) {
		len = strlen(query);
		snprintf(query + len, 2048 - len,
		         " LIMIT %d", limit);
	}

	PGresult *res = PQexecParams(
		dbc->dbc->conn, query,
		pi, NULL, (const char**)params, NULL, NULL, 0);
	chkret(res, PQclear(res); for (int i = 0; i < 24; i++) sfree(params[i]); sfree(query));

	sfree(query);

	_icur_t *icur = salloc(sizeof(_icur_t));

	int num = PQntuples(res);
	icur->nrows = num;
	icur->rows = salloc(num * sizeof(json_t*));

	for (int i = 0; i < num; i++) {
		icur->rows[i] = json_loads(PQgetvalue(res, i, 0), 0, NULL);
	}

	PQclear(res);

	for (int i = 0; i < 24; i++) {
		sfree(params[i]);
	}

	db_set_error(NULL);
	return icur;
}

static int _postgres_ds_count(ds_t *ds, const char ***condition) {
	dbc_t *dbc = ds->dbc;

	char *query = salloc(2048);
	char *params[24] = { 0 };

	snprintf(query, 2048,
	         "SELECT"
	         "  COUNT(*)"
	         " FROM %s"
	         " WHERE ",
	         ds->ds->tbl_name);

	int pi = 0;
	size_t len = strlen(query);
	translate_cond(condition, &query[len], 2048 - len, params, 24, &pi);

	PGresult *res = PQexecParams(
		dbc->dbc->conn, query,
		pi, NULL, (const char**)params, NULL, NULL, 0);
	chkret_r(res, PQclear(res); for (int i = 0; i < 24; i++) sfree(params[i]); sfree(query), -1);

	sfree(query);

	int n = atoi(PQgetvalue(res, 0, 0));

	PQclear(res);

	for (int i = 0; i < 24; i++) {
		sfree(params[i]);
	}

	db_set_error(NULL);
	return n;
}

static void _postgres_ds_close(ds_t *ds) {
	sfree(ds->ds->tbl_name);
	sfree(ds->ds);
	db_set_error(NULL);
}
