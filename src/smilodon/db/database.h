/**
 * @file
 * @brief Database functions.
 */

#pragma once

#include <jansson.h>
#include <stddef.h>

/// @brief Database backends.
enum db_backend {
	/// @brief PostgreSQL database backend.
	BACKEND_POSTGRES = 0,

	/// @brief Number of database backends.
	BACKEND__N,
};

#define TYPE_NULL (char*)1
#define TYPE_STRING (char*)2
#define TYPE_OBJECT (char*)3
#define TYPE_ARRAY (char*)4

#define NOT (char*)1
#define OFTYPE (char*)2
#define EXISTS (char*)3
#define EQ_STRING (char*)4
#define LT (char*)5
#define LE (char*)6

#define COND_TRUE (const char**[]){ NULL }
#define CNDL(...) (const char**[]){ __VA_ARGS__, NULL }
#define CND(...) (const char*[]){ __VA_ARGS__ }
#define OPT1(f, a) IS_EXIST(a), f(a)
#define OPT2(f, a, b) IS_EXIST(a), f(a, b)

#define IS_NULL(x) CND(#x, OFTYPE, TYPE_NULL)
#define ISNT_NULL(x) CND(#x, NOT, OFTYPE, TYPE_NULL)
#define IS_STRING(x) CND(#x, OFTYPE, TYPE_STRING)
#define ISNT_STRING(x) CND(#x, NOT, OFTYPE, TYPE_STRING)
#define IS_OBJECT(x) CND(#x, OFTYPE, TYPE_OBJECT)
#define ISNT_OBJECT(x) CND(#x, NOT, OFTYPE, TYPE_OBJECT)
#define IS_ARRAY(x) CND(#x, OFTYPE, TYPE_ARRAY)
#define ISNT_ARRAY(x) CND(#x, NOT, OFTYPE, TYPE_ARRAY)
#define IS_EXIST(x) CND(#x, EXISTS)
#define ISNT_EXIST(x) CND(#x, NOT, EXISTS)
#define ISEQ_STR(x, y) CND(#x, EQ_STRING, y)
#define ISNTEQ_STR(x, y) CND(#x, NOT, EQ_STRING, y)
#define IS_LT(x, y) CND(#x, LT, y)
#define ISNT_LT(x, y) CND(#x, NOT, LT, y)
#define IS_LE(x, y) CND(#x, LE, y)
#define ISNT_LE(x, y) CND(#x, NOT, LE, y)
#define IS_GT(x, y) IS_LT(y, x)
#define ISNT_GT(x, y) ISNT_LT(y, x)
#define IS_GE(x, y) IS_LE(y, x)
#define ISNT_GE(x, y) ISNT_LE(y, x)

#define FAIL IS_EXIST($$meta._fail)

/// @brief Database connection. Structure is backend-dependent.
typedef struct backend_dbc _idbc_t;

/// @brief Data store. Structure is backend-dependent.
typedef struct backend_ds _ids_t;

/// @brief Cursor. Structure is backend dependent.
typedef struct backend_cur _icur_t;

/// @brief Database transaction element.
typedef struct transaction {
	/// @brief Data source.
	struct ds *ds;

	/// @brief Condition array.
	const char ***condition;

	/// @brief Object.
	json_t *object;

	/// @brief Next.
	struct transaction *cdr;
} transaction_t;

/// @brief Database connection.
typedef struct dbc {
	/// @brief Backend type.
	enum db_backend backend;

	/// @brief Data stores.
	struct ds_list *ds_list;

	/// @brief Backend database connection.
	_idbc_t *dbc;
} dbc_t;

/// @brief Data store definition.
typedef struct ds_def {
	/// @brief Name.
	const char *name;

	/// @brief Constraints, of the form [preconditions, constraint, NULL]. Null terminated.
	const char ***constraints;

	/// @brief Indices, list of fields. Null terminated.
	const char **indices;
} ds_def_t;

/// @brief Data store object.
typedef struct ds {
	/// @brief Associated database connection.
	dbc_t *dbc;

	/// @brief Data store definition.
	ds_def_t *def;

	/// @brief Backend data store.
	_ids_t *ds;
} ds_t;

/// @brief Linked list of data stores.
struct ds_list {
	/// @brief Data store.
	ds_t *car;

	/// @brief Next.
	struct ds_list *cdr;
};

/// @brief Result cursor.
typedef struct cur {
	/// @brief Database connection.
	dbc_t *dbc;

	/// @brief Backend cursor.
	_icur_t *cur;
} cur_t;

/**
 * @brief Open a database connection.
 * @param backend Database backend.
 * @param conn_string Connection string. Structure is backend-dependent.
 * @return Pointer to the opened database connection, or NULL in case of error.
 */
dbc_t *dbc_open(enum db_backend backend, const char *conn_string);

/**
 * @brief Close a database connection, its associated datastores, and its associated cursors.
 * @param dbc Database connection.
 */
void dbc_close(dbc_t *dbc);

/**
 * @brief Initialize a transaction.
 * @param dbc Database connection.
 * @return Transaction.
 */
transaction_t *trans_init(dbc_t *dbc);

/**
 * @brief Commit and free a transaction.
 * @param trans Transaction.
 */
void trans_commit(transaction_t *trans);

/**
 * @brief Rollback and free a transaction.
 * @param trans Transaction.
 */
void trans_rollback(transaction_t *trans);

/**
 * @brief Open a data store, possibly creating or updating it.
 * @param dbc Database connection.
 * @param def Data store definition.
 * @return Data store.
 */
ds_t *ds_open(dbc_t *dbc, ds_def_t *def);

/**
 * @brief Replace entries matching a condition.
 * @param ds Data store.
 * @param trans Transaction.
 * @param condition Condition, or NULL to insert (see also ds_insert()).
 * @param object Object, or NULL to delete (see also ds_delete()).
 */
void ds_update(ds_t *ds, transaction_t **trans, const char ***condition, json_t *object);

/**
 * @brief Insert an entry.
 * @param ds Data store.
 * @param trans Transaction.
 * @param object Object.
 */
#define ds_insert(ds, trans, object) ds_update(ds, trans, NULL, object)

/**
 * @brief Delete entries.
 * @param ds Data store.
 * @param trans Transaction.
 * @param condition Condition.
 */
#define ds_delete(ds, trans, condition) ds_update(ds, trans, condition, NULL)

/**
 * @brief Select rows ordered by a key.
 * @param ds Data store.
 * @param condition Condition.
 * @param key Key to order by, or NULL for unspecified ordering.
 * @param descending If 1, will be ordered in descending order instead of ascending.
 * Ignored if key is NULL.
 * @param limit Limit to the number of rows fetched, or -1 for no limit.
 * @return Cursor, or NULL in case of error.
 */
cur_t *ds_select_ordered(ds_t *ds, const char ***condition,
                         const char *key, char descending,
                         int limit);

/**
 * @brief Select some entries.
 * @param ds Data store.
 * @param condition Condition.
 * @return Cursor, or NULL in case of error.
 */
cur_t *ds_select(ds_t *ds, const char ***condition);

/**
 * @brief Count the number of matching rows.
 * @param ds Data store.
 * @param condition Condition.
 * @return Number of rows, or -1 in case of error.
 */
int ds_count(ds_t *ds, const char ***condition);

/**
 * @brief Select one row.
 * @param ds Data store.
 * @param condition Condition.
 * @return Row, or NULL in case of error.
 */
json_t *ds_select_one(ds_t *ds, const char ***condition);

/**
 * @brief Close a data store.
 * @param ds Data store.
 */
void ds_close(ds_t *ds);

/**
 * @brief Get the number of rows.
 * @param cur Cursor.
 * @return Number of rows, or -1 in case of error or if it cannot be determined.
 */
int cur_nrows(cur_t *cur);

/**
 * @brief Get a row in a cursor.
 * @param cur Cursor.
 * @param row Row number.
 * @return The row, or -1 if the number is out of bound.
 */
json_t *cur_get(cur_t *cur, int row);

/**
 * @brief Close a cursor.
 * @param cur Cursor.
 */
void cur_close(cur_t *cur);

/**
 * @brief Get the last error message.
 * @return Pointer to the error message, or NULL if there are no errors. Do not free it.
 */
const char *db_get_error();

/**
 * @brief Set the error message.
 * @param msg Error message.
 */
void db_set_error(const char *msg);

/**
 * @brief Check if there was an error, potentially logging it.
 * @param errexit If 1, exit the program in case of error.
 * @return 1 in case there was an error, else 0.
 */
char db_check_error(char errexit);
