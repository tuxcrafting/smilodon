#include "smilodon/db/database.h"

#include <jansson.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include "smilodon/db/backend.h"
#include "smilodon/db/backends/postgres/postgres.h"
#include "smilodon/util.h"

// error message. thread-local to guarantee thread safety.
static __thread const char *g_error_msg;

static struct backend_functions *backends[] = {
	&postgres_functions,
};

// parse a JSON path and return the matching object in a JSON object.
static char get_jpath(const char *jpath, json_t *obj, json_t **ret) {
	size_t b = 0;
	size_t len = 0;
	for (size_t i = 0;; i++) {
		if (jpath[i] == '.' || jpath[i] == '\0') {
			char *buf = salloc(len + 1);
			memcpy(buf, &jpath[b], len);
			obj = json_object_get(obj, buf);
			if (obj == NULL) {
				sfree(buf);
				return 0;
			}
			sfree(buf);

			b = i + 1;
			len = 0;

			if (jpath[i] == '\0') {
				break;
			}
		} else {
			len++;
		}
	}
	*ret = obj;
	return 1;
}

// directly check a condition.
static const char *check_cond(const char ***condition, size_t len, json_t *obj) {
	json_t *o;
	for (size_t i = 0; condition[i] != NULL && i < len; i++) {
		size_t j = 0;
		char invert = 0;
		const char *a = condition[i][j];
		const char *c = condition[i][j + 1];
	check_cond_s:
		switch ((size_t)c) {
		case (size_t)NOT:
			invert = 1;
			c = condition[i][j + 2];
			j++;
			goto check_cond_s;
		case (size_t)OFTYPE:
			if (!get_jpath(a, obj, &o)) {
				return "OFTYPE: key doesn't exist";
			}
			switch ((size_t)condition[i][j + 2]) {
			case (size_t)TYPE_NULL:
				if (!(json_is_null(o) ^ invert)) {
					if (invert) {
						return "OFTYPE: object is null";
					} else {
						return "OFTYPE: object is not null";
					}
				}
				break;
			case (size_t)TYPE_STRING:
				if (!(json_is_string(o) ^ invert)) {
					if (invert) {
						return "OFTYPE: object is string";
					} else {
						return "OFTYPE: object is not string";
					}
				}
				break;
			case (size_t)TYPE_OBJECT:
				if (!(json_is_object(o) ^ invert)) {
					if (invert) {
						return "OFTYPE: object is object";
					} else {
						return "OFTYPE: object is not object";
					}
				}
				break;
			case (size_t)TYPE_ARRAY:
				if (!(json_is_array(o) ^ invert)) {
					if (invert) {
						return "OFTYPE: object is array";
					} else {
						return "OFTYPE: object is not array";
					}
				}
				break;
			}
			j += 3;
			break;
		case (size_t)EXISTS:
			if (!(get_jpath(a, obj, &o) ^ invert)) {
				if (invert) {
					return "EXISTS: key exists";
				} else {
					return "EXISTS: key doesn't exist";
				}
			}
			j += 2;
			break;
		case (size_t)EQ_STRING:
			if (!get_jpath(a, obj, &o)) {
				return "EQ_STRING: key doesn't exist";
			}
			if (!json_is_string(o)) {
				return "EQ_STRING: object is not string";
			}
			if ((strcmp(json_string_value(o), condition[i][j + 2]) != 0) ^ invert) {
				if (invert) {
					return "EQ_STRING: string equal";
				} else {
					return "EQ_STRING: string not equal";
				}
			}
			j += 3;
			break;
		}
	}
	return NULL;
}

// deallocate a transaction.
static void clear_transaction(transaction_t *u) {
	while (u != NULL) {
		if (u->object != NULL) {
			json_decref(u->object);
		}
		transaction_t *n = u->cdr;
		sfree(u);

		u = n;
	}
}

dbc_t *dbc_open(enum db_backend backend, const char *conn_string) {
	if (backend < 0 || backend >= BACKEND__N) {
		return NULL;
	}

	_idbc_t *idbc = backends[backend]->dbc_open(conn_string);
	if (idbc == NULL) {
		return NULL;
	}

	dbc_t *dbc = salloc(sizeof(dbc_t));
	dbc->backend = backend;
	dbc->dbc = idbc;
	return dbc;
}

static void _ds_close_nodel(ds_t *ds);

void dbc_close(dbc_t *dbc) {
	backends[dbc->backend]->dbc_close(dbc);

	struct ds_list *l = dbc->ds_list;
	while (l != NULL) {
		_ds_close_nodel(l->car);
		l = l->cdr;
	}

	sfree(dbc);
}

transaction_t *trans_init(dbc_t *dbc) {
	db_set_error(NULL);
	return NULL;
}

void trans_commit(transaction_t *trans) {
	if (trans == NULL) {
		db_set_error(NULL);
		return;
	}
	dbc_t *dbc = trans->ds->dbc;

	if (backends[dbc->backend]->trans_commit(trans) == NULL) {
		return;
	}

	clear_transaction(trans);
	db_set_error(NULL);
}

void trans_rollback(transaction_t *trans) {
	clear_transaction(trans);
	db_set_error(NULL);
}

ds_t *ds_open(dbc_t *dbc, ds_def_t *def) {
	_ids_t *ids = backends[dbc->backend]->ds_open(dbc, def);
	if (ids == NULL) {
		return NULL;
	}

	ds_t *ds = salloc(sizeof(ds_t));
	ds->dbc = dbc;
	ds->def = def;
	ds->ds = ids;

	struct ds_list *l = salloc(sizeof(struct ds_list));
	l->car = ds;
	l->cdr = dbc->ds_list;
	dbc->ds_list = l;

	return ds;
}

void ds_update(ds_t *ds, transaction_t **trans, const char ***condition, json_t *object) {
	dbc_t *dbc = ds->dbc;

	if (*trans != NULL && (*trans)->ds->dbc != dbc) {
		db_set_error("Transactions must be limited to one database connection.");
		return;
	}

	transaction_t *u = salloc(sizeof(transaction_t));
	u->ds = ds;
	u->condition = condition;
	u->object = object;

	if (object == NULL) {
		if (condition == NULL) {
			sfree(u);
			db_set_error("Cannot use NULL condition with NULL object.");
			return;
		}
	} else {
		size_t i = 0;
		for (;;) {
			size_t _i = i;
			size_t len = 0;
			for (; ds->def->constraints[i] != NULL; i++) {
				len++;
			}
			i = _i;
			if (len-- == 0) {
				break;
			}

			if (check_cond(&ds->def->constraints[i], len, object) == NULL) {
				const char *s;
				if ((s = check_cond(&ds->def->constraints[i += len], 1, object)) != NULL) {
					sfree(u);
					char *errbuf = aprintf("Invalid object: %s on %s.", s,
					                       ds->def->constraints[i][0]);
					db_set_error(errbuf);
					sfree(errbuf);
					return;
				}
			} else {
				i += len;
			}
			i += 2;
		}

		json_incref(object);
	}

	transaction_t **last = trans;
	while (*last != NULL) {
		last = &(*last)->cdr;
	}
	*last = u;

	db_set_error(NULL);
}

cur_t *ds_select_ordered(ds_t *ds, const char ***condition,
                         const char *key, char descending,
                         int limit) {
	_icur_t *icur = backends[ds->dbc->backend]->ds_select_ordered(
		ds, condition, key, descending, limit);
	if (icur == NULL) {
		return NULL;
	}

	cur_t *cur = salloc(sizeof(cur_t));
	cur->dbc = ds->dbc;
	cur->cur = icur;
	return cur;
}

cur_t *ds_select(ds_t *ds, const char ***condition) {
	return ds_select_ordered(ds, condition, NULL, 0, -1);
}

json_t *ds_select_one(ds_t *ds, const char ***condition) {
	cur_t *cur = ds_select(ds, condition);
	if (cur == NULL) {
		return NULL;
	}
	if (cur_nrows(cur) == 0) {
		db_set_error(NULL);
		cur_close(cur);
		return NULL;
	}
	json_t *obj = cur_get(cur, 0);
	cur_close(cur);
	return obj;
}

int ds_count(ds_t *ds, const char ***condition) {
	return backends[ds->dbc->backend]->ds_count(ds, condition);
}

static void _ds_close_nodel(ds_t *ds) {
	backends[ds->dbc->backend]->ds_close(ds);
	if (db_get_error() != NULL) {
		return;
	}
	sfree(ds);
}

void ds_close(ds_t *ds) {
	_ds_close_nodel(ds);

	struct ds_list *prev = NULL;
	struct ds_list **cur = &ds->dbc->ds_list;
	for (;;) {
		if ((*cur)->car == ds) {
			prev->cdr = (*cur)->cdr;
			sfree(*cur);
			*cur = prev;
			break;
		}
		prev = *cur;
		cur = &(*cur)->cdr;
	}
}

int cur_nrows(cur_t *cur) {
	return backends[cur->dbc->backend]->cur_nrows(cur);
}

json_t *cur_get(cur_t *cur, int row) {
	return backends[cur->dbc->backend]->cur_get(cur, row);
}

void cur_close(cur_t *cur) {
	backends[cur->dbc->backend]->cur_close(cur);
	if (db_get_error() != NULL) {
		return;
	}
	sfree(cur);
}

const char *db_get_error() {
	return g_error_msg;
}

void db_set_error(const char *msg) {
	sfree((char*)g_error_msg);
	if (msg == NULL) {
		g_error_msg = msg;
	} else {
		g_error_msg = sstrdup(msg);
	}
}

char db_check_error(char err_exit) {
	if (db_get_error() != NULL) {
		syslog(LOG_ERR, "%s", db_get_error());
		if (err_exit) {
			exit(1);
		}
		return 1;
	}
	return 0;
}
