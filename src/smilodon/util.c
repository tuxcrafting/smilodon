#include "smilodon/util.h"

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <util/neo_err.h>
#include <util/neo_str.h>

void *(*alloc_fn)(size_t n);
void (*free_fn)(void *p);
void *(*realloc_fn)(void *p, size_t n);
char *(*strdup_fn)(const char *s);

uint64_t get_num_id() {
	return ((time(NULL) & 0xFFFFFFFFFF) << 16) | (rand() & 0xFFFF);
}

void check_nerr(NEOERR *err) {
	if (err != STATUS_OK) {
		STRING str;
		string_init(&str);
		nerr_error_string(err, &str);
		syslog(LOG_ERR, "%.*s", str.len, str.buf);
		string_clear(&str);
		exit(1);
	}
}

char *strchrnul(const char *s, int c) {
	char *p = strchr(s, c);
	if (p == NULL) {
		p = (char*)s + strlen(s);
	}
	return p;
}

char *aprintf(const char *format, ...) {
	va_list ap;
	va_start(ap, format);
	size_t len = 0;
	char *buf = NULL;
	for (;;) {
		len += 1024;
		buf = srealloc(buf, len);
		size_t w = vsnprintf(buf, len, format, ap);
		if (w < len) {
			break;
		}
	}
	va_end(ap);
	return buf;
}

void *memdup(void *p, size_t n) {
	void *d = salloc(n);
	memcpy(d, p, n);
	return d;
}
