/**
 * @file
 * @brief Internal API to handle common operations.
 */

#include <jansson.h>

/// @brief Internal API error codes.
enum iapi_error {
	/// @brief Success.
	IAPI_SUCCESS = 0,

	/// @brief Unspecified error.
	IAPI_ERROR,

	/// @brief Object not found.
	IAPI_NOTFOUND,

	/// @brief Error while handling activity.
	IAPI_ACTIVITY_INBOX,

	/// @brief Username is invalid.
	IAPI_ACCOUNT_INVALID_NAME,

	/// @brief Password is empty.
	IAPI_ACCOUNT_EMPTY_PASSWORD,

	/// @brief User already exists.
	IAPI_ACCOUNT_EXISTS,

	/// @brief Content empty.
	IAPI_SUBMIT_EMPTY_CONTENT,

	/// @brief Content too long.
	IAPI_SUBMIT_LONG_CONTENT,
};

/**
 * @brief Handle an activity.
 * @param activity Activity object. Reference count is decremented.
 * @return Error code.
 */
enum iapi_error iapi_handle_activity(json_t *activity);

/**
 * @brief Do an action (like, announce) on an object.
 * @param actor Actor object of the author.
 * @param object Object.
 * @param id_base Identifier base name.
 * @param action Action.
 * @return Error code.
 */
enum iapi_error iapi_object_action(json_t *actor, json_t *object, const char *id_base, const char *action);

/**
 * @brief Like an object.
 * @param actor Actor object of the author.
 * @param object Object.
 * @return Error code.
 */
#define iapi_object_like(actor, object) iapi_object_action(actor, object, "like", "Like")

/**
 * @brief Boost an object.
 * @param actor Actor object of the author.
 * @param object Object.
 * @return Error code.
 */
#define iapi_object_boost(actor, object) iapi_object_action(actor, object, "boost", "Announce")

/**
 * @brief Create an account.
 * @param username Username.
 * @param password Password.
 * @return Error code.
 */
enum iapi_error iapi_create_account(const char *username, const char *password);

/**
 * @brief Submit a note.
 * @param actor Actor object of the author.
 * @param parent Parent of the reply, or NULL for top-level.
 * @param content Content.
 * @return Error code.
 */
enum iapi_error iapi_submit_note(json_t *actor, const char *parent, const char *content);
