#include "smilodon/iapi/iapi.h"

#include <argon2.h>
#include <inttypes.h>
#include <jansson.h>
#include <openssl/bio.h>
#include <openssl/bn.h>
#include <openssl/evp.h>
#include <openssl/ossl_typ.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "smilodon/database.h"
#include "smilodon/date.h"
#include "smilodon/db/database.h"
#include "smilodon/config.h"
#include "smilodon/util.h"
#include "smilodon/web/federation/federator.h"
#include "smilodon/web/federation/inbox.h"
#include "smilodon/web/server.h"
#include "smilodon/web/template.h"
#include "smilodon/web/webfinger.h"

enum iapi_error iapi_handle_activity(json_t *activity) {
	enum iapi_error r = IAPI_SUCCESS;
	if (inbox_handle_activity(activity)) {
		federate(activity);
	} else {
		r = IAPI_ACTIVITY_INBOX;
	}
	json_decref(activity);
	return r;
}

enum iapi_error iapi_object_action(json_t *actor, json_t *object, const char *id_base, const char *action) {
	char published_buf[64];
	to_iso8601(time(NULL), published_buf, 0, 0, 0);

	json_t *activity = json_pack(
		"{sssssOsosOsss[sO]s[O]}",
		"@context", "https://www.w3.org/ns/activitystreams",
		"type", action,
		"actor", json_object_get(actor, "id"),
		"id", json_sprintf("https://%s/%s.json?id=%" PRIu64, server_name, id_base, get_num_id()),
		"object", json_object_get(object, "id"),
		"published", published_buf,
		"to",
		"https://www.w3.org/ns/activitystreams#Public",
		json_object_get(object, "attributedTo"),
		"cc", json_object_get(actor, "followers"));

	return iapi_handle_activity(activity);
}

// validate an username: [A-Za-z0-9_-]{1,127}
static char validate_username(const char *username) {
	for (int i = 0; username[i] != 0; i++) {
		char c = username[i];
		if (!((c >= 'A' && c <= 'Z') ||
		      (c >= 'a' && c <= 'z') ||
		      (c >= '0' && c <= '9') ||
		      c == '_' || c == '-')) {
			return 0;
		}
		if (i > 127) {
			return 0;
		}
	}
	return username[0] != 0;
}

// generate a 2048-bit RSA key pair
static void generate_keypair(char **pubkey, char **privkey) {
	BIGNUM *bne = BN_new();
	RSA *rsa = RSA_new();
	BIO *pubkey_bio = BIO_new(BIO_s_mem());
	BIO *privkey_bio = BIO_new(BIO_s_secmem());

	// generate the RSA key.
	BN_set_word(bne, RSA_F4);
	RSA_generate_key_ex(rsa, 2048, bne, NULL);

	// write the PEMs.
	PEM_write_bio_RSA_PUBKEY(pubkey_bio, rsa);
	// private key is secured with 256-bit AES-CTR using the application secret key.
	PEM_write_bio_RSAPrivateKey(privkey_bio, rsa, EVP_aes_256_ctr(),
	                            (uint8_t*)secret, strlen(secret), NULL, NULL);

	// deallocate RSA data.
	RSA_free(rsa);
	BN_free(bne);

	// get the buffer data.
	char *pubkey_ptr, *privkey_ptr;
	int pubkey_len = BIO_get_mem_data(pubkey_bio, &pubkey_ptr);
	int privkey_len = BIO_get_mem_data(privkey_bio, &privkey_ptr);

	// allocate and copy the output buffers.
	*pubkey = salloc(pubkey_len + 1);
	memcpy(*pubkey, pubkey_ptr, pubkey_len);
	*privkey = salloc(privkey_len + 1);
	memcpy(*privkey, privkey_ptr, privkey_len);

	// free the BIOs.
	BIO_free(pubkey_bio);
	BIO_free(privkey_bio);
}

enum iapi_error iapi_create_account(const char *username, const char *password) {
	// check username is valid.
	if (!validate_username(username)) {
		return IAPI_ACCOUNT_INVALID_NAME;
	}

	// check password isn't empty.
	if (password[0] == 0) {
		return IAPI_ACCOUNT_EMPTY_PASSWORD;
	}

	// check there isn't already an user with this name.
	json_t *obj = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR(username, username)));
	db_check_error(1);

	if (obj != NULL) {
		json_decref(obj);
		return IAPI_ACCOUNT_EXISTS;
	}

	// hash the password.
	uint8_t salt[16];
	RAND_bytes(salt, 16);

	char *encoded = salloc(256);
	argon2id_hash_encoded(3, 1024, 1,
	                      password, strlen(password),
	                      salt, 16,
	                      32, encoded, 256);

	uint64_t id = get_num_id();
	char id_buf[32];
	snprintf(id_buf, 32, "%" PRIu64, id);

	// generate the key pair.
	char *pubkey;
	char *privkey;
	generate_keypair(&pubkey, &privkey);

	// set the various HDF properties.
	HDF *hdf;
	check_nerr(hdf_init(&hdf));
	check_nerr(hdf_set_value(hdf, "server_name", server_name));
	check_nerr(hdf_set_value(hdf, "public_key", pubkey));
	check_nerr(hdf_set_value(hdf, "private_key", privkey));
	check_nerr(hdf_set_value(hdf, "id", id_buf));
	check_nerr(hdf_set_value(hdf, "username", username));
	check_nerr(hdf_set_value(hdf, "password", encoded));

	// create the user object.
	char *buf;
	render_template_buf(&buf, hdf, "templates/user.json", 0);
	json_t *user = json_loads(buf, 0, NULL);
	sfree(buf);

	// create the actor object.
	render_template_buf(&buf, hdf, "templates/actor.json", 0);
	json_t *actor = json_loads(buf, 0, NULL);
	sfree(buf);

	// insert them.
	transaction_t *trans = trans_init(dbc);
	db_check_error(1);

	ds_insert(datastores[DS_USERS], &trans, user);
	db_check_error(1);

	ds_insert(datastores[DS_ACTORS], &trans, actor);
	db_check_error(1);

	trans_commit(trans);
	db_check_error(1);

	// free everything.
	json_decref(user);
	json_decref(actor);
	sfree(encoded);
	sfree(pubkey);
	sfree(privkey);
	hdf_destroy(&hdf);

	return IAPI_SUCCESS;
}

#define WSPACE	  \
	"\x01\x02\x03\x04\x05\x06\x07" \
	"\x08\x0A\x0B\x0C\x0D\x0E\x0F" \
	"\x11\x12\x13\x14\x15\x16\x17" \
	"\x18\x1A\x1B\x1C\x1D\x1E\x1F" \
	"\x20"

enum iapi_error iapi_submit_note(json_t *actor, const char *parent, const char *content) {
	// error if the content is empty.
	if (content[0] == 0) {
		return IAPI_SUBMIT_EMPTY_CONTENT;
	}

	// or if it's too long.
	if (strlen(content) > atoi(config_get("max_post_length", "131072"))) {
		return IAPI_SUBMIT_LONG_CONTENT;
	}

	// note id.
	uint64_t id = get_num_id();
	char id_buf[32];
	snprintf(id_buf, 32, "%" PRIu64, id);

	// time.
	char published_buf[64];
	to_iso8601(time(NULL), published_buf, 0, 0, 0);

	// parse mentions.
	json_t *to = json_array();
	json_array_append_new(to, json_string("https://www.w3.org/ns/activitystreams#Public"));
	json_t *tag = json_array();
	char *at = strchr(content, '@');
	while (at != NULL) {
		// find a mention, split into username and domain.
		at++;
		size_t l = strcspn(at, WSPACE "@");
		if (l == 0) {
			continue;
		}

		char *username = salloc(l + 1);
		memcpy(username, at, l);
		at += l;

		char *domain;
		if (*at != '@') {
			domain = sstrdup(server_name);
		} else {
			at++;
			l = strcspn(at, WSPACE "@");
			domain = salloc(l + 1);
			memcpy(domain, at, l);
			at += l;
		}

		// find the associated actor and add it to the list.
		json_t *obj = webfinger(username, domain);
		if (obj != NULL) {
			json_array_append(to, json_object_get(obj, "id"));

			char *s = aprintf("@%s@%s", username, domain);
			json_array_append_new(
				tag,
				json_pack("{sssssO}",
				          "type", "Mention",
				          "name", s,
				          "href", json_object_get(obj, "id")));

			sfree(s);
			json_decref(obj);
		}

		sfree(username);
		sfree(domain);
		at = strchr(at, '@');
	}
	char *to_s = json_dumps(to, 0);
	json_decref(to);

	// set the HDF parameters.
	HDF *hdf;
	check_nerr(hdf_init(&hdf));
	check_nerr(hdf_set_value(hdf, "server_name", server_name));
	check_nerr(hdf_set_value(hdf, "id", id_buf));
	check_nerr(hdf_set_value(hdf, "content", content));
	check_nerr(hdf_set_value(hdf, "user.actor_id", json_object_string(actor, "id")));
	check_nerr(hdf_set_value(hdf, "user.actor_followers", json_object_string(actor, "followers")));
	check_nerr(hdf_set_value(hdf, "published", published_buf));
	check_nerr(hdf_set_value(hdf, "to", to_s));

	sfree(to_s);

	// create activity object.
	char *buf;
	render_template_buf(&buf, hdf, "templates/note-activity.json", 0);
	json_t *activity = json_loads(buf, 0, NULL);
	if (json_array_size(tag) == 0) {
		json_decref(tag);
	} else {
		json_object_set_new(json_object_get(activity, "object"), "tag", tag);
	}
	if (parent != NULL) {
		json_t *parent_obj = ds_select_one(
			datastores[DS_OBJECTS],
			CNDL(ISEQ_STR($$meta.id, parent)));
		db_check_error(1);
		if (parent_obj == NULL) {
			return IAPI_NOTFOUND;
		}
		json_object_set(
			json_object_get(activity, "object"),
			"inReplyTo", json_object_get(parent_obj, "id"));
		json_decref(parent_obj);
	}
	sfree(buf);

	hdf_destroy(&hdf);

	// insert it.
	return iapi_handle_activity(activity);
}
