/**
 * @file
 * @brief Configuration file parser. Uses the HDF format.
 */

#pragma once

/// @brief Application secret.
extern const char *secret;

/**
 * @brief Load a configuration file.
 * @param filename Filename.
 */
void config_load(const char *filename);

/**
 * @brief Get a configuration entry.
 * @param name Entry name.
 * @param def Default value.
 * @return The value, the default value or exits in case the default is (char*)-1.
 */
const char *config_get(const char *name, const char *def);
