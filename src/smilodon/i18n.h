/**
 * @file
 * @brief Internationalization functions.
 */

#pragma once

#include <util/neo_hdf.h>

/// @brief HDF containing the content of langs/root.hdf.
HDF *langs;

/**
 * @brief Initialize the internationalization things.
 */
void i18n_init();

/**
 * @brief Set an HDF array with the language metadata.
 * @param hdf HDF.
 * @param prefix The subtree path.
 */
void i18n_fill_meta(HDF *hdf, const char *prefix);

/**
 * @brief Get a translated text in a locale.
 * @param text Text identifier.
 * @param locale Locale, or NULL for the system locale.
 * @return Translated text.
 */
const char *i18n_gettext(const char *text, const char *locale);
