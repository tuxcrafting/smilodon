#include "smilodon/database.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include "smilodon/config.h"
#include "smilodon/db/database.h"
#include "smilodon/i18n.h"

dbc_t *dbc;
ds_t *datastores[DS__N];

ds_def_t datastore_defs[DS__N] = {
	// DS_USER
	{
		.name = "Users",
		.constraints = (const char**[]){
			IS_STRING(username), NULL,
			IS_STRING(actor), NULL,
			IS_STRING(password), NULL,
			IS_STRING(private_key), NULL,
			NULL
		},
		.indices = (const char*[]){
			"$$meta.id",
			"username",
			"actor",
			NULL
		},
	},

	// DS_ACTORS
	{
		.name = "Actors",
		.constraints = (const char**[]){
			IS_STRING(id), NULL,
			ISEQ_STR(type, "Person"), NULL,

			IS_STRING(preferredUsername), NULL,
			IS_STRING(inbox), NULL,

			OPT1(IS_STRING, url), NULL,

			IS_EXIST(name), ISNT_NULL(name), IS_STRING(name), NULL,
			IS_EXIST(summary), ISNT_NULL(summary), IS_STRING(summary), NULL,

			IS_EXIST(icon), IS_OBJECT(icon), ISNT_NULL(icon.url), IS_STRING(icon.url), NULL,
			IS_EXIST(icon), ISNT_OBJECT(icon), ISNT_NULL(icon), IS_STRING(icon), NULL,

			OPT1(IS_OBJECT, endpoints), NULL,
			IS_EXIST(endpoints), IS_STRING(endpoints.sharedInbox), NULL,

			IS_OBJECT(publicKey), NULL,
			IS_STRING(publicKey.id), NULL,
			IS_STRING(publicKey.owner), NULL,
			IS_STRING(publicKey.publicKeyPem), NULL,

			NULL
		},
		.indices = (const char*[]){
			"$$meta.id",
			"$$meta.domain",
			"id",
			"publicKey.id",
			"preferredUsername",
			NULL
		},
	},

	// DS_ACTIVITIES
	{
		.name = "Activities",
		.constraints = (const char**[]){
			IS_STRING(id), NULL,
			IS_STRING(actor), NULL,
			IS_STRING(type), NULL,

			ISEQ_STR(type, "Create"), IS_STRING(published), NULL,
			ISEQ_STR(type, "Create"), IS_OBJECT(object), NULL,
			ISEQ_STR(type, "Like"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Announce"), IS_STRING(published), NULL,
			ISEQ_STR(type, "Announce"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Follow"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Accept"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Reject"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Undo"), IS_STRING(object), IS_STRING(object), NULL,
			ISEQ_STR(type, "Undo"), ISNT_STRING(object), IS_OBJECT(object), NULL,
			ISEQ_STR(type, "Undo"), IS_OBJECT(object), IS_STRING(object.id), NULL,

			ISNTEQ_STR(type, "Create"),
			ISNTEQ_STR(type, "Like"),
			ISNTEQ_STR(type, "Announce"),
			ISNTEQ_STR(type, "Follow"),
			ISNTEQ_STR(type, "Accept"),
			ISNTEQ_STR(type, "Reject"),
			ISNTEQ_STR(type, "Undo"), FAIL, NULL,

			NULL

		},
		.indices = (const char*[]){
			"$$meta.id",
			"$$meta.inserted",
			"id",
			"actor",
			"object",
			NULL
		},
	},

	// DS_OBJECTS
	{
		.name = "Objects",
		.constraints = (const char**[]){
			IS_STRING(id), NULL,
			IS_STRING(attributedTo), NULL,
			IS_STRING(type), NULL,

			IS_EXIST(to), IS_ARRAY(to), NULL,
			IS_EXIST(cc), IS_ARRAY(cc), NULL,

			IS_EXIST(inReplyTo), ISNT_NULL(inReplyTo), IS_STRING(inReplyTo), NULL,
			IS_EXIST(tag), ISNT_NULL(tag), IS_ARRAY(tag), NULL,

			ISEQ_STR(type, "Note"), IS_STRING(content), NULL,
			ISEQ_STR(type, "Announce"), IS_STRING(object), NULL,

			ISNTEQ_STR(type, "Note"),
			ISNTEQ_STR(type, "Announce"), FAIL, NULL,

			NULL
		},
		.indices = (const char*[]){
			"$$meta.id",
			"$$meta.inserted",
			"id",
			"attributedTo",
			"inReplyTo",
			NULL
		},
	},

	// DS_INSTANCES
	{
		.name = "Instances",
		.constraints = (const char**[]){
			IS_STRING(domain), NULL,
			IS_STRING(webfinger), NULL,
			NULL
		},
		.indices = (const char*[]){
			"domain",
			NULL
		},
	},

	// DS_NOTIFICATIONS
	{
		.name = "Notifications",
		.constraints = (const char**[]){
			IS_STRING(id), NULL,
			IS_STRING(actor), NULL,
			IS_STRING(source), NULL,
			IS_STRING(inserted), NULL,
			IS_STRING(read), NULL,
			ISEQ_STR(type, "Mention"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Like"), IS_STRING(object), NULL,
			ISEQ_STR(type, "Boost"), IS_STRING(object), NULL,
			NULL
		},
		.indices = (const char*[]){
			"id",
			"actor",
			"inserted",
			"read",
			NULL
		},
	},

	// DS_FOLLOWS
	{
		.name = "Follows",
		.constraints = (const char**[]){
			IS_STRING(actor), NULL,
			IS_ARRAY(followers), NULL,
			IS_ARRAY(followings), NULL,
			NULL
		},
		.indices = (const char*[]){
			"actor",
			NULL
		},
	},
};

void database_open() {
	// parse the backend.
	int backend;
	const char *backend_s = config_get("database.backend", (char*)-1);

	if (strcmp(backend_s, "postgres") == 0) {
		backend = BACKEND_POSTGRES;
	} else {
		syslog(LOG_ERR, "%s%s%s",
		       i18n_gettext("smilodon.database.unknown_backend_a", NULL),
		       backend_s,
		       i18n_gettext("smilodon.database.unknown_backend_b", NULL));
		exit(1);
	}

	syslog(LOG_INFO, "%s", i18n_gettext("smilodon.database.opening", NULL));

	// open the database.
	dbc = dbc_open(backend, config_get("database.connection_string", (char*)-1));
	db_check_error(1);

	// open the data stores.
	for (int i = 0; i < DS__N; i++) {
		datastores[i] = ds_open(dbc, &datastore_defs[i]);
		db_check_error(1);
	}
}

void database_close() {
	if (dbc != NULL) {
		syslog(LOG_INFO, "%s", i18n_gettext("smilodon.database.closing", NULL));
		dbc_close(dbc);
		db_check_error(1);
	}
}
