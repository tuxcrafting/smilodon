/**
 * @file
 * @brief Global database functions.
 */

#pragma once

#include "smilodon/db/database.h"

enum {
	/// @brief Users.
	DS_USERS = 0,

	/// @brief Actors.
	DS_ACTORS,

	/// @brief Activities.
	DS_ACTIVITIES,

	/// @brief Objects.
	DS_OBJECTS,

	/// @brief Instances.
	DS_INSTANCES,

	/// @brief Notifications.
	DS_NOTIFICATIONS,

	/// @brief Follows.
	DS_FOLLOWS,

	/// @brief Number of data stores.
	DS__N,
};

/// @brief Global database connection.
extern dbc_t *dbc;

/// @brief Data store pointers.
extern ds_t *datastores[DS__N];

/// @brief Data store definitions.
extern ds_def_t datastore_defs[DS__N];

/**
 * @brief Initialize the database.
 */
void database_open();

/**
 * @brief Close the database.
 */
void database_close();
