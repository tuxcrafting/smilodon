/**
 * @file
 * @brief Basic utility functions.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>
#include <util/neo_err.h>

/// @brief Allocation function.
extern void *(*alloc_fn)(size_t n);

/// @brief Deallocation function.
extern void (*free_fn)(void *p);

/// @brief Reallocation function.
extern void *(*realloc_fn)(void *p, size_t n);

/// @brief Strdup function.
extern char *(*strdup_fn)(const char *s);

/// @see alloc_fn
#define salloc (*alloc_fn)

/// @see free_fn
#define sfree (*free_fn)

/// @see realloc_fn
#define srealloc (*realloc_fn)

/// @see strdup_fn
#define sstrdup (*strdup_fn)

/**
 * @brief Create a numeric ID.
 *
 * The top 48 bits are the seconds since the UNIX epoch,
 * and the bottom 16 bits are a random integer.
 * @return ID.
 */
uint64_t get_num_id();

/**
 * @brief Check a NEOERR, log and exit in case there's an error.
 * @param err Error.
 */
void check_nerr(NEOERR *err);

/**
 * @brief Similar to strchr() but returns the poiner of the null byte if c is not found.
 * @param s String.
 * @param c Character.
 * @return Pointer to the first instance of the character.
 */
char *strchrnul(const char *s, int c);

/**
 * @brief printf() in a dynamically allocated buffer.
 * @param format Format string.
 * @param ... Parameters.
 * @return Dynamically allocated buffer.
 */
char *aprintf(const char *format, ...);

/**
 * @brief Duplicate a region of memory.
 * @param p Base pointer.
 * @param n Area size.
 * @return Newly allocated copy.
 */
void *memdup(void *p, size_t n);

/**
 * @brief Get the string value of a field in a JSON object.
 * @param obj JSON object.
 * @param name Name of the field.
 * @return String value.
 */
#define json_object_string(obj, name) json_string_value(json_object_get(obj, name))
