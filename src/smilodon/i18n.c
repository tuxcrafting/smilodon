#include "smilodon/i18n.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <util/neo_files.h>
#include <util/neo_hdf.h>

#include "smilodon/datadir.h"
#include "smilodon/util.h"

HDF *langs;
static HDF *texts;
static char *glocale;

static HDF *open_hdf(HDF *modify, const char *path, char soft) {
	char *path_buf = datadir_path(path);
	char *contents;

	HDF *hdf;
	if (modify == NULL) {
		check_nerr(hdf_init(&hdf));
	} else {
		hdf = modify;
	}

	struct stat st;
	if (soft && stat(path_buf, &st) < 0) {
		sfree(path_buf);
		return hdf;
	}

	check_nerr(ne_load_file(path_buf, &contents));
	sfree(path_buf);

	check_nerr(hdf_read_string(hdf, contents));
	free(contents);

	return hdf;
}

void i18n_init() {
	check_nerr(hdf_init(&texts));

	// parse root.hdf and root.override.hdf.
	HDF *root = open_hdf(NULL, "langs/root.hdf", 0);
	open_hdf(root, "langs/root.override.hdf", 1);

	// iterate over all supported languages.
	check_nerr(hdf_get_node(root, "langs", &langs));
	HDF *lang = hdf_obj_child(langs);
	while (lang != NULL) {
		char *lang_name = hdf_obj_name(lang);

		// load the lang files.
		HDF *lang_t;
		check_nerr(hdf_get_node(texts, lang_name, &lang_t));
		HDF *lang_a = hdf_get_obj(lang, "0");
		while (lang_a != NULL) {
			open_hdf(lang_t, hdf_obj_value(lang_a), 0);
			lang_a = hdf_obj_next(lang_a);
		}

		lang = hdf_obj_next(lang);
	}

	// get the locale.
	const char *lang_name = getenv("LANG");
	if (lang_name == NULL) {
		glocale = "en";
		return;
	}

	char lcode[3] = { 0 };
	memcpy(lcode, lang_name, 2);

	if (hdf_get_obj(texts, lcode) != NULL) {
		glocale = salloc(3);
		memcpy(glocale, lcode, 2);
	}
}

void i18n_fill_meta(HDF *hdf, const char *prefix) {
	check_nerr(hdf_get_node(hdf, prefix, &hdf));

	int i = 0;
	char buf[32];

	HDF *lang = hdf_obj_child(langs);
	while (lang != NULL) {
		snprintf(buf, 32, "%d.shortcode", i);
		check_nerr(hdf_set_value(hdf, buf, hdf_obj_name(lang)));

		snprintf(buf, 32, "%d.eng_name", i);
		check_nerr(hdf_set_value(hdf, buf, hdf_get_value(lang, "eng_name", NULL)));

		snprintf(buf, 32, "%d.nat_name", i);
		check_nerr(hdf_set_value(hdf, buf, hdf_get_value(lang, "nat_name", NULL)));

		i++;
		lang = hdf_obj_next(lang);
	}
}

const char *i18n_gettext(const char *text, const char *locale) {
	if (locale == NULL) {
		locale = glocale;
	}
	HDF *node = hdf_get_obj(texts, locale);
	char *t = hdf_get_value(node, text, NULL);
	if (t == NULL) {
		if (strcmp(locale, "en") == 0) {
			return text;
		}
		return i18n_gettext(text, "en");
	}

	// remove trailing \ .
	int len = strlen(t);
	if (t[len - 1] == '\\') {
		t[len - 1] = 0;
	}

	return t;
}
