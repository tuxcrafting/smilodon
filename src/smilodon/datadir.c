#include "smilodon/datadir.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <limits.h>

#include "smilodon/config.h"
#include "smilodon/util.h"

// base directory.
static char *datadir_base;

void datadir_init() {
	const char *path = config_get("data_directory", (char*)-1);

	char *buf = salloc(PATH_MAX);
	if (realpath(path, buf) == NULL) {
		syslog(LOG_ERR, "Couldn't expand path '%s'.", path);
		exit(1);
	}
	if (buf[strlen(buf) - 1] != '/') {
		buf[strlen(buf)] = '/';
	}

	datadir_base = buf;
}

char *datadir_path(const char *filename) {
	char *buf = salloc(PATH_MAX);

	strncpy(buf, datadir_base, PATH_MAX);
	int buf_len = strlen(buf);
	strncpy(buf + buf_len, filename, PATH_MAX - buf_len);

	return buf;
}
