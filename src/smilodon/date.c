#include "smilodon/date.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// week days.
static char *days[] = {
	"Sun", "Mon", "Tue", "Wed",
	"Thu", "Fri", "Sat",
};

// months.
static char *months[] = {
	"Jan", "Feb", "Mar", "Apr",
	"May", "Jun", "Jul", "Aug",
	"Sep", "Oct", "Nov", "Dec",
};

void to_rfc822(time_t t, char *buf) {
	// break down time.
	struct tm tm;
	gmtime_r(&t, &tm);

	// generate the date.
	sprintf(buf,
	        "%s, %02d %s %04d %02d:%02d:%02d GMT",
	        days[tm.tm_wday],
	        tm.tm_mday, months[tm.tm_mon], tm.tm_year + 1900,
	        tm.tm_hour, tm.tm_sec, tm.tm_min);
}

time_t parse_iso8601(const char *buf) {
	int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;

	// this is bad. fix this.
	sscanf(buf, "%d-%d-%dT%d:%d:%d",
	       &year, &month, &day,
	       &hour, &min, &sec);

	struct tm tm = {
		.tm_year = year - 1900,
		.tm_mon = month - 1,
		.tm_mday = day,
		.tm_hour = hour,
		.tm_min = min,
		.tm_sec = sec
	};
	return timegm(&tm);
}

void to_iso8601(time_t t, char *buf, int tz_h, int tz_m, char nice) {
	t += tz_h * 3600 + (tz_m * 60) * (tz_h < 0 ? -1 : 1);
	struct tm tm;
	gmtime_r(&t, &tm);

	int year = tm.tm_year + 1900;
	int month = tm.tm_mon + 1;
	int day = tm.tm_mday;
	int hour = tm.tm_hour;
	int min = tm.tm_min;
	int sec = tm.tm_sec;

	if (nice) {
		sprintf(buf,
		        "%04d-%02d-%02d %02d:%02d:%02d %c%02d:%02d",
		        year, month, day, hour, min, sec, tz_h < 0 ? '-' : '+', abs(tz_h), tz_m);
	} else {
		sprintf(buf,
		        "%04d-%02d-%02dT%02d:%02d:%02d",
		        year, month, day, hour, min, sec);
		int len = strlen(buf);
		if (tz_h != 0 || tz_m != 0) {
			sprintf(buf + len,
			        "+%02d:%02d",
			        tz_h, tz_m);
		} else {
			sprintf(buf + len, "Z");
		}
	}
}
