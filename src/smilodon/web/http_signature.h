/**
 * @file
 * @brief HTTP signature verification and creation.
 */

#pragma once

#include <util/neo_hdf.h>

/**
 * @brief Verify an HTTP signature.
 * @param hdf HDF containing the request data.
 * @param actor_id Actor ID of the author, or NULL to accept any author.
 * @return 1 if valid, 0 otherwise.
 */
char http_verify(HDF *hdf, const char *actor_id);

/**
 * @brief Create an HTTP signature.
 * @param actor_id Actor ID of the author.
 * @param url URL to sign.
 * @param body The POST data.
 * @return Pointer to the header list, or NULL in case of error.
 */
char **http_sign(const char *actor_id, const char *url, const char *body);
