/**
 * @file
 * @brief ClearSilver wrappers.
 */

#pragma once

#include <fcgiapp.h>
#include <util/neo_hdf.h>

/**
 * @brief Parse parameters in a string with a specified format.
 * @param hdf HDF.
 * @param value Value to parse.
 * @param prefix Path in the HDF for where to store the parameters.
 * @param type The type: 0 for URL, 1 for form, 2 for cookies, 3 for HTTP signatures.
 */
void parse_parameters(HDF *hdf, const char *value, const char *prefix, char type);

/**
 * @brief Create an HDF, filling it with request data.
 * @param req FastCGI request.
 * @return The created HDF, or NULL on error.
 */
HDF *create_hdf(FCGX_Request *req);

/**
 * @brief Render a template.
 * @param req FastCGI request.
 * @param hdf HDF.
 * @param filename Template filename.
 */
#define render_template(req, hdf, filename) render_template_fcgi(req, hdf, filename, 1)

/**
 * @brief Render a template to FastCGI.
 * @param req FastCGI request.
 * @param hdf HDF.
 * @param filename Template filename.
 * @param minify Minify the generated code.
 */
void render_template_fcgi(FCGX_Request *req, HDF *hdf, char *filename, char minify);

/**
 * @brief Render a template to a buffer.
 * @param buf Buffer.
 * @param hdf HDF.
 * @param filename Template filename.
 * @param minify Minify the generated code.
 */
void render_template_buf(char **buf, HDF *hdf, char *filename, char minify);
