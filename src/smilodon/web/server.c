#include "smilodon/web/server.h"

#include <errno.h>
#include <fcgiapp.h>
#include <jansson.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <util/neo_hdf.h>

#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/datadir.h"
#include "smilodon/date.h"
#include "smilodon/db/database.h"
#include "smilodon/config.h"
#include "smilodon/i18n.h"
#include "smilodon/web/federation/inbox.h"
#include "smilodon/web/frontend/frontend.h"
#include "smilodon/web/nodeinfo.h"
#include "smilodon/web/template.h"
#include "smilodon/web/well_known.h"
#include "smilodon/util.h"

const char *server_name;

// FastCGI socket file descriptor.
static int sockfd;

// server state.
static int state;
#define STATE_PREINIT 0
#define STATE_RUNNING 1
#define STATE_SHUTDOWN 2

static char in_http_error;

static int msg_pipe[2];

static struct pollfd pollfds[2];
#define NUM_POLL_FD (sizeof(pollfds) / sizeof(struct pollfd))

#define QUEUE_SIZE 512
static pthread_mutex_t queue_mutex;
static struct queue_element {
	char used;
	void (*function)(void*, void*);
	void *a;
	void *b;
	pthread_t thread;
	struct queue_element *next;
} queue[QUEUE_SIZE];
static struct queue_element *head, *tail;

char enqueue_callback(void (*function)(void*, void*), void *a, void *b, pthread_t thread) {
	pthread_mutex_lock(&queue_mutex);
	struct queue_element *elm = NULL;
	for (int i = 0; i < QUEUE_SIZE; i++) {
		if (!queue[i].used) {
			elm = &queue[i];
			break;
		}
	}
	if (elm == NULL) {
		pthread_mutex_unlock(&queue_mutex);
		return 0;
	}
	elm->used = 1;
	elm->function = function;
	elm->a = a;
	elm->b = b;
	elm->thread = thread;
	elm->next = NULL;
	if (head == NULL) {
		head = elm;
	} else {
		tail->next = elm;
	}
	tail = elm;
	pthread_mutex_unlock(&queue_mutex);
	char buf[1] = { 'a' };
	write(msg_pipe[1], buf, 1);
	return 1;
}

// server HTTP callback.
static void do_http() {
	// initialize the request object.
	FCGX_Request hreq;
	FCGX_InitRequest(&hreq, sockfd, 0);
	FCGX_Request *req = &hreq;

	// accept a request.
	if (FCGX_Accept_r(req) < 0) {
		syslog(LOG_WARNING, "%s", i18n_gettext("smilodon.server.fcgi_socket_err", NULL));
		in_http_error++;
		if (in_http_error == 5) {
			stop_server();
		}
		return;
	}

	fprintf(stderr,
	        "[%s] %s %s\n",
	        FCGX_GetParam("REMOTE_ADDR", req->envp),
	        FCGX_GetParam("REQUEST_METHOD", req->envp),
	        FCGX_GetParam("REQUEST_URI", req->envp));

	HDF *hdf = create_hdf(req);
	char *path = hdf_get_value(hdf, "req.path", "/");
	if (strcmp(path, "/") == 0) {
		redirect(req, "301 Moved Permanently", "/web");
	} else if (strcmp(path, "/actor.json") == 0) {
		json_t *obj = ds_select_one(
			datastores[DS_ACTORS],
			CNDL(ISEQ_STR(id, hdf_get_value(hdf, "req.uri", ""))));
		db_check_error(1);
		if (obj == NULL) {
			http_error(req, "404 Not Found");
			goto actor_end;
		}
		json_object_del(obj, "$$meta");
		char *s = json_dumps(obj, JSON_COMPACT);
		json_decref(obj);
		FCGX_FPrintF(req->out,
		             "Content-Type: " LD_JSON_AS "\r\n\r\n%s",
		             s);
		sfree(s);
	actor_end:;
	} else if (strcmp(path, "/activity.json") == 0) {
		json_t *obj = ds_select_one(
			datastores[DS_ACTIVITIES],
			CNDL(ISEQ_STR(id, hdf_get_value(hdf, "req.uri", ""))));
		db_check_error(1);
		if (obj == NULL) {
			http_error(req, "404 Not Found");
			goto activity_end;
		}
		json_object_del(obj, "$$meta");
		char *s = json_dumps(obj, JSON_COMPACT);
		json_decref(obj);
		FCGX_FPrintF(req->out,
		             "Content-Type: " LD_JSON_AS "\r\n\r\n%s",
		             s);
		sfree(s);
	activity_end:;
	} else if (strcmp(path, "/object.json") == 0) {
		if (strstr(hdf_get_value(hdf, "req.headers.accept", ""), "json") != NULL) {
			json_t *obj = ds_select_one(
				datastores[DS_OBJECTS],
				CNDL(ISEQ_STR(id, hdf_get_value(hdf, "req.uri", ""))));
			db_check_error(1);
			if (obj == NULL) {
				http_error(req, "404 Not Found");
				goto object_end;
			}
			json_object_del(obj, "$$meta");
			char *s = json_dumps(obj, JSON_COMPACT);
			json_decref(obj);
			FCGX_FPrintF(req->out,
			             "Content-Type: " LD_JSON_AS "\r\n\r\n%s",
			             s);
			sfree(s);
		} else {
			json_t *obj = ds_select_one(
				datastores[DS_OBJECTS],
				CNDL(ISEQ_STR(id, hdf_get_value(hdf, "req.uri", ""))));
			db_check_error(1);
			if (obj == NULL) {
				http_error(req, "404 Not Found");
				goto object_end;
			}
			char *s = aprintf("/web/note?id=%s",
			                  json_object_string(
				                  json_object_get(obj, "$$meta"), "id"));
			json_decref(obj);
			redirect(req, "303 See Other", s);
			sfree(s);
		}
	object_end:;
	} else if (strcmp(path, "/shared_inbox.json") == 0 ||
	           strcmp(path, "/inbox.json") == 0) {
		inbox_handle(req, hdf);
	} else if (strstr(path, "/web") == path) {
		webfe_handle(req, hdf);
	} else if (strstr(path, "/.well-known") == path) {
		wellknown_handle(req, hdf);
	} else if (strstr(path, "/nodeinfo") == path) {
		nodeinfo_handle(req, hdf);
	} else if (strcmp(path, "/favicon.ico") == 0) {
		char *s = datadir_path("static/favicon.ico");
		FILE *f = fopen(s, "rb");
		sfree(s);
		char *buf = salloc(1024);
		int len = fread(buf, 1, 1024, f);
		fclose(f);
		FCGX_FPrintF(req->out,
		             "Content-Type: image/x-icon\r\n\r\n");
		FCGX_PutStr(buf, len, req->out);
		sfree(buf);
	} else {
		http_error(req, "404 Not Found");
	}
	hdf_destroy(&hdf);

	FCGX_FFlush(req->out);
	FCGX_Finish_r(req);
}

void start_server(const char *path, int worker_n, int backlog) {
	if (backlog < 1) {
		syslog(LOG_ERR, "%s", i18n_gettext("smilodon.server.backlog_min", NULL));
	}

	pipe(msg_pipe);

	// load server_name.
	server_name = config_get("server_name", (char*)-1);

	syslog(LOG_INFO, "%s", i18n_gettext("smilodon.server.fcgi_start", NULL));

	// initialize FastCGI and create socket.
	sockfd = FCGX_OpenSocket(path, backlog);
	if (sockfd < 0) {
		syslog(LOG_ERR, "%s", i18n_gettext("smilodon.server.fcgi_err_socker", NULL));
		exit(1);
	}
	FCGX_Init();

	// indicates server is (almost) ready.
	state = STATE_RUNNING;

	// set poll fds.
	pollfds[0].fd = sockfd;
	pollfds[0].events = POLLIN;
	pollfds[1].fd = msg_pipe[0];
	pollfds[1].events = POLLIN;

	char cont = 1;
	while (cont) {
		// poll, return on error.
		int r = poll(pollfds, NUM_POLL_FD, -1);
		if (r < 0) {
			if (errno != EINTR || state == STATE_SHUTDOWN) {
				cont = 0;
			}
			continue;
		}

		// process fds.
		for (int i = 0; i < NUM_POLL_FD; i++) {
			if (pollfds[i].revents != 0) {
				if (pollfds[i].fd == sockfd) {
					do_http();
				} else if (pollfds[i].fd == msg_pipe[0]) {
					char buf[1];
					read(msg_pipe[0], buf, 1);
				}
				pollfds[i].revents = 0;
			}
		}

		// process callbacks.
		pthread_mutex_lock(&queue_mutex);
		struct queue_element *elm = head;
		while (elm != NULL) {
			elm->function(elm->a, elm->b);
			pthread_join(elm->thread, NULL);
			elm->used = 0;
			elm = elm->next;
		}
		head = NULL;
		pthread_mutex_unlock(&queue_mutex);
	}
}

void stop_server() {
	if (state != STATE_RUNNING) {
		// don't do anything in case the server isn't initialized.
		return;
	}

	// tell the server to shutdown.
	state = STATE_SHUTDOWN;
	close(sockfd);
}

void redirect(FCGX_Request *req, const char *status, const char *location) {
	char *l = (char*)location;
	char free_l = 0;
	if (location[0] != 'h') {
		l = aprintf("https://%s%s", server_name, l);
		free_l = 1;
	}
	FCGX_FPrintF(
		req->out,
		"Status: %s\r\n"
		"Location: %s\r\n"
		"Content-Type: text/html\r\n"
		"\r\n"
		"<a href=\"%s\">%s</a>",
		status, l, l, status);
	if (free_l) {
		sfree(l);
	}
}

void http_error(FCGX_Request *req, const char *status) {
	FCGX_FPrintF(
		req->out,
		"Status: %s\r\n"
		"Content-Type: text/html\r\n"
		"\r\n"
		"%s",
		status, status);
}

void cookie_set(FCGX_Request *req, const char *name, const char *value,
                const char *path, time_t expiration) {
	FCGX_FPrintF(
		req->out,
		"Set-Cookie: %s=%s",
		name, value);
	if (expiration != (time_t)-1) {
		char buf[256];
		to_rfc822(expiration, buf);
		FCGX_FPrintF(
			req->out,
			"; Expires=%s",
			buf);
	}
	FCGX_FPrintF(
		req->out,
		"; Domain=%s; Path=%s; Secure; HttpOnly\r\n",
		server_name, path);
}
