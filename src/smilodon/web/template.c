#include "smilodon/web/template.h"

#include <cgi/cgi.h>
#include <cs/cs.h>
#include <fcgiapp.h>
#include <jansson.h>
#include <libxml/HTMLparser.h>
#include <libxml/HTMLtree.h>
#include <libxml/parser.h>
#include <libxml/xmlsave.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/neo_err.h>
#include <util/neo_files.h>
#include <util/neo_hdf.h>

#include "html_attrs.gperf.h"
#include "html_tags.gperf.h"
#include "smilodon/datadir.h"
#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"

void parse_parameters(HDF *hdf, const char *value, const char *prefix, char type) {
	check_nerr(hdf_get_node(hdf, prefix, &hdf));

	char *buf = salloc(strlen(value) + 3);
	strcpy(buf, value);

	const char *delim = type == 2 ? "; " : "&";
	if (type == 3) {
		delim = ",";
	}

	char *saveptr = buf, *tok;
	while ((tok = strtok_r(NULL, delim, &saveptr)) != NULL) {
		char *eq = strchr(tok, '=');
		if (eq == NULL) {
			continue;
		}
		*eq = 0;
		// ignore parameter if it has a null length.
		if (eq == tok) {
			goto ignore_query_insert;
		}
		char *qval = eq + 1;

		int i, j;
		for (i = 0, j = 0; qval[i] != 0; i++, j++) {
			if (qval[i] == '%' && type < 2) {
				char c = qval[++i];
				char n = 0;
				if (c >= '0' && c <= '9') {
					n |= c - '0';
				} else if (c >= 'a' && c <= 'f') {
					n |= c - 'a' + 10;
				} else if (c >= 'A' && c <= 'F') {
					n |= c - 'A' + 10;
				} else {
					goto ignore_query_insert;
				}
				n <<= 4;
				c = qval[++i];
				if (c >= '0' && c <= '9') {
					n |= c - '0';
				} else if (c >= 'a' && c <= 'f') {
					n |= c - 'a' + 10;
				} else if (c >= 'A' && c <= 'F') {
					n |= c - 'A' + 10;
				} else {
					goto ignore_query_insert;
				}
				qval[j] = n;
			} else if (qval[i] == '+' && type == 1) {
				qval[j] = ' ';
			} else if (qval[i] == '\n' && type == 1) {
				j--;
			} else {
				qval[j] = qval[i];
			}
		}
		qval[j] = 0;

		if (type == 3) {
			// check format and strip quotes.
			if (strlen(qval) < 2 || qval[0] != '"' || qval[j - 1] != '"') {
				goto ignore_query_insert;
			}
			qval++;
			qval[j - 2] = 0;
		}

		check_nerr(hdf_set_value(hdf, tok, qval));
	ignore_query_insert:;
		*eq = '=';
	}

	sfree(buf);
}

HDF *create_hdf(FCGX_Request *req) {
	HDF *hdf;
	check_nerr(hdf_init(&hdf));

	// parse the environment
	for (int i = 0; req->envp[i] != NULL; i++) {
		char *value = strchr(req->envp[i], '=');
		int key_len = (int)(value - req->envp[i]);
		value++;
		char *key = salloc(key_len + 1);
		memcpy(key, req->envp[i], key_len);

		if (strstr(key, "HTTP_") == key) {
			// it's an HTTP header, lowercase it and store it in req.headers.
			char *buf = salloc(13 + strlen(key + 5));
			strcpy(buf, "req.headers.");
			for (int i = 5, j = 12; key[i - 1] != 0; i++, j++) {
				if (key[i] >= 'A' && key[i] <= 'Z') {
					buf[j] = key[i] ^ (1 << 5);
				} else {
					buf[j] = key[i];
				}
			}
			check_nerr(hdf_set_value(hdf, buf, value));
			sfree(buf);

			// if it's the Cookie header, store all the cookies in req.cookies.
			if (strcmp(key, "HTTP_COOKIE") == 0) {
				parse_parameters(hdf, value, "req.cookies", 2);
			}
		} else if (strcmp(key, "REQUEST_METHOD") == 0) {
			// store the method in req.method.
			check_nerr(hdf_set_value(hdf, "req.method", value));
		} else if (strcmp(key, "DOCUMENT_URI") == 0) {
			// store the URI in req.path.
			int len = strlen(value);
			char *buf = salloc(len + 1);
			strcpy(buf, value);
			if (buf[len - 1] == '/' && len > 1) {
				buf[len - 1] = 0;
			}
			check_nerr(hdf_set_value(hdf, "req.path", buf));
			sfree(buf);
		} else if (strcmp(key, "REQUEST_URI") == 0) {
			// store the full URI in req.uri.
			int len = strlen(server_name) + strlen(value) + 9;
			char *buf = salloc(len);
			strcpy(buf, "https://");
			strcpy(buf + 8, server_name);
			strcpy(buf + strlen(buf), value);
			check_nerr(hdf_set_value(hdf, "req.uri", buf));
			sfree(buf);
		} else if (strcmp(key, "CONTENT_LENGTH") == 0) {
			// store the request body in req.body.data and the length in req.body.length.
			int n = atoi(value);
			if (n <= 0) {
				check_nerr(hdf_set_int_value(hdf, "req.body.length", 0));
				check_nerr(hdf_set_value(hdf, "req.body.data", ""));
			} else {
				check_nerr(hdf_set_int_value(hdf, "req.body.length", n));
				// calloc instead of salloc because hdf_destroy will use stdlib free.
				char *buf = calloc(n + 1, 1);
				FCGX_GetStr(buf, n, req->in);
				check_nerr(hdf_set_buf(hdf, "req.body.data", buf));

				// in case the content type is application/x-www-form-urlencoded,
				// parse parameters in req.form.
				char *content_type = FCGX_GetParam("HTTP_CONTENT_TYPE", req->envp);
				if (content_type != NULL &&
				    strcmp(content_type, "application/x-www-form-urlencoded") == 0) {
					parse_parameters(hdf, buf, "req.form", 1);
				}
			}
		} else if (strcmp(key, "QUERY_STRING") == 0) {
			// store the various query parameters in req.query.
			parse_parameters(hdf, value, "req.query", 0);
		}

		sfree(key);
	}

	return hdf;
}

struct cs_buf_data {
	char *buf;
	int len;
	int i;
};

static NEOERR *csoutfunc(void *ctx, char *str) {
	struct cs_buf_data *data = ctx;

	for (int i = 0; str[i] != 0; i++) {
		if (data->i == data->len) {
			data->buf = srealloc(data->buf, data->len += 1024);
		}
		data->buf[data->i++] = str[i];
	}
	if (data->i == data->len) {
		data->buf = srealloc(data->buf, data->len += 1024);
	}
	data->buf[data->i] = 0;

	return STATUS_OK;
}

static NEOERR *csfileload(void *ctx, HDF *hdf, const char *filename, char **contents) {
	char *path_buf = datadir_path(filename);
	NEOERR *err = ne_load_file(path_buf, contents);
	sfree(path_buf);
	return err;
}

static NEOERR *cs_json_escape(const char *in, char **out) {
	json_t *j = json_string(in);
	char *s = json_dumps(j, JSON_ENCODE_ANY);
	*out = calloc(strlen(s) - 1, 1);
	memcpy(*out, s + 1, strlen(s) - 2);
	sfree(s);
	json_decref(j);
	return STATUS_OK;
}

static void sanitize_node(xmlNodePtr node) {
	if (node == NULL || xmlNodeIsText(node)) {
		return;
	}

	// strip disallowed tags.
	if (tags_in_word_set((char*)node->name, strlen((char*)node->name)) == NULL) {
		xmlUnlinkNode(node);
		xmlFreeNode(node);
		return;
	}

	// strip disallowed attributes.
	xmlAttrPtr attr = node->properties;
	while (attr != NULL) {
		if (attrs_in_word_set((char*)attr->name, strlen((char*)attr->name)) == NULL) {
			xmlAttrPtr old = attr;
			attr = attr->next;
			xmlUnsetProp(node, old->name);
			continue;
		}

		attr = attr->next;
	}

	// loop on children.
	xmlNodePtr ch = node->children;
	while (ch != NULL) {
		xmlNodePtr next = ch->next;
		sanitize_node(ch);
		ch = next;
	}
}

static NEOERR *cs_html_sanitize(const char *in, char **out) {
	// wrap in <p>.
	char *buf = aprintf("<p>%s</p>", in);

	// parse document.
	htmlDocPtr doc = htmlReadDoc((xmlChar*)buf, "", "utf-8",
	                             HTML_PARSE_NODEFDTD |
	                             HTML_PARSE_NOERROR |
	                             HTML_PARSE_NOWARNING |
	                             HTML_PARSE_NONET |
	                             HTML_PARSE_NOIMPLIED);
	sfree(buf);
	xmlNodePtr root = xmlDocGetRootElement(doc);
	sanitize_node(root);

	// dump document.
	char *mem;
	int size;

	htmlDocDumpMemory(doc, (xmlChar**)&mem, &size);
	char *_mem = mem;

	xmlFreeDoc(doc);

	// remove libxml2 artefacts.
	mem += 3;
	size -= 8;

	// copy output.
	*out = memdup(mem, size + 1);
	(*out)[size] = 0;
	sfree(_mem);
	return STATUS_OK;
}

static NEOERR *cs_attr_sanitize(const char *in, char **out) {
	*out = calloc(512, 1);
	char *outp = *out;
	int i = 0;
	while (*in != 0 && i < 511) {
		switch (*in) {
		case '<':
			strncpy(&outp[i], "&lt;", 512 - i);
			i += 4;
			break;
		case '&':
			strncpy(&outp[i], "&amp;", 512 - i);
			i += 5;
			break;
		case '"':
			strncpy(&outp[i], "&quot;", 512 - i);
			i += 6;
			break;
		case '\'':
			strncpy(&outp[i], "&#39;", 512 - i);
			i += 5;
			break;
		default:
			outp[i++] = *in;
		}
		in++;
	}
	return STATUS_OK;
}

// for fuck's sake.
static __thread char *gettext_locale;

static NEOERR *cs_gettext(const char *in, char **out) {
	const char *text = i18n_gettext(in, gettext_locale);
	*out = malloc(strlen(text) + 1);
	strcpy(*out, text);
	return STATUS_OK;
}

void render_template_fcgi(FCGX_Request *req, HDF *hdf, char *filename, char minify) {
	char *buf;
	render_template_buf(&buf, hdf, filename, minify);
	FCGX_PutS(buf, req->out);
	sfree(buf);
}

void render_template_buf(char **buf, HDF *hdf, char *filename, char minify) {
	CSPARSE *cs;
	gettext_locale = hdf_get_value(hdf, "locale", NULL);

	// init the template.
	check_nerr(cs_init(&cs, hdf));

	// change the file load function to one using datadir.
	cs_register_fileload(cs, NULL, csfileload);

	// register useful functions.
	check_nerr(cgi_register_strfuncs(cs));
	check_nerr(cs_register_strfunc(cs, "gettext", cs_gettext));
	check_nerr(cs_register_strfunc(cs, "json_escape", cs_json_escape));
	check_nerr(cs_register_strfunc(cs, "html_sanitize", cs_html_sanitize));
	check_nerr(cs_register_strfunc(cs, "attr_sanitize", cs_attr_sanitize));

	// parse the template.
	check_nerr(cs_parse_file(cs, filename));

	struct cs_buf_data data = { 0 };

	// render it.
	check_nerr(cs_render(cs, &data, csoutfunc));

	*buf = data.buf;

	// try to minify.
	if (minify) {
		if (strstr(filename, ".json") != NULL) {
			json_t *json = json_loads(*buf, JSON_DECODE_ANY, NULL);
			sfree(*buf);
			*buf = json_dumps(json, JSON_COMPACT | JSON_ENCODE_ANY);
			json_decref(json);
		} else if (strstr(filename, ".xml") != NULL) {
			xmlDocPtr doc = xmlParseMemory(*buf, strlen(*buf));
			xmlBufferPtr buffer = xmlBufferCreate();
			xmlSaveCtxtPtr ctx = xmlSaveToBuffer(buffer, NULL, 0);
			xmlSaveDoc(ctx, doc);
			xmlSaveClose(ctx);
			xmlFreeDoc(doc);
			sfree(*buf);
			*buf = (char*)xmlBufferDetach(buffer);
			xmlBufferFree(buffer);
		}
	}

	cs_destroy(&cs);
}
