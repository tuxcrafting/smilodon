#include "smilodon/web/frontend/frontend.h"

#include <fcgiapp.h>
#include <inttypes.h>
#include <jansson.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <util/neo_hdf.h>

#include "build_config.h"
#include "smilodon/activitypub/actor.h"
#include "smilodon/auth.h"
#include "smilodon/database.h"
#include "smilodon/date.h"
#include "smilodon/db/database.h"
#include "smilodon/iapi/iapi.h"
#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/federation/federator.h"
#include "smilodon/web/federation/inbox.h"
#include "smilodon/web/server.h"
#include "smilodon/web/template.h"
#include "smilodon/web/webfinger.h"

#define get_title(page) i18n_gettext("web." #page ".title", locale)
#define set_title(title) check_nerr(hdf_set_value(hdf, "title", title))

static void form_error(FCGX_Request *req, HDF *hdf, festate_t *state, char is_post,
                       const char *base, int (*func)(FCGX_Request*, HDF*, festate_t*), char *template,
                       const char *title) {
	if (is_post) {
		int n = func(req, hdf, state);
		if (n == 1) {
			redirect(req, "303 See Other", "/web");
		} else if (n == 0) {
			http_error(req, "400 Bad Request");
		} else {
			char c = strchr(base, '?') == NULL ? '?' : '&';
			char *buf = aprintf("/web/%s%cerr=%d", base, c, n);
			redirect(req, "303 See Other", buf);
			sfree(buf);
		}
	} else {
		set_title(title);
		FCGX_FPrintF(req->out, "Content-Type: text/html\r\n\r\n");
		render_template(req, hdf, template);
	}
}

static void get_actor_data(const char *id, char **dname, char **durl) {
	json_t *actor = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(id, id)));

	const char *preferredUsername = actor == NULL ? "erroruser" :
		json_object_string(actor, "preferredUsername");
	const char *domain = actor == NULL ? "error.example" :
		json_object_string(json_object_get(actor, "$$meta"), "domain");

	char *name;
	if (json_object_get(actor, "name") != NULL) {
		name = sstrdup(json_object_string(actor, "name"));
	} else {
		name = aprintf("@%s@%s", preferredUsername, domain);
	}
	*dname = name;

	char *url;
	if (strcmp(domain, server_name) == 0) {
		url = aprintf("https://%s/web/user?username=%s",
		              server_name, preferredUsername);
	} else {
		url = aprintf("https://%s/web/user?username=%s@%s",
		              server_name, preferredUsername, domain);
	}
	*durl = url;

	if (actor != NULL) {
		json_decref(actor);
	}
}

static char do_note(HDF *hdf, festate_t *state, const char *prefix, json_t *row,
                    char show_replies, char hide_private) {
	char buf[64];

	check_nerr(hdf_set_value(hdf, "server_name", server_name));

	if (hide_private) {
		// check it is addressed to as:Public.
		int public = 0;
		json_t *list = json_array();
		json_array_extend(list, json_object_get(row, "to"));
		json_array_extend(list, json_object_get(row, "cc"));
		json_t *obj;
		int i;
		json_array_foreach(list, i, obj) {
			if (json_is_string(obj) &&
			    (strcmp(json_string_value(obj),
			            "https://www.w3.org/ns/activitystreams#Public") == 0 ||
			     strcmp(json_string_value(obj),
			            "as:Public") == 0 ||
			     strcmp(json_string_value(obj),
			            "Public") == 0)) {
				public = 1;
				break;
			}
		}
		json_decref(list);
		if (!public) {
			return 0;
		}
	}

	char *name, *url;
	get_actor_data(json_object_string(row, "attributedTo"), &name, &url);
	snprintf(buf, 64, "%s.author.name", prefix);
	check_nerr(hdf_set_value(hdf, buf, name));
	snprintf(buf, 64, "%s.author.url", prefix);
	check_nerr(hdf_set_value(hdf, buf, url));
	sfree(name);
	sfree(url);

	json_t *meta = json_object_get(row, "$$meta");
	char dbuf[64];
	time_t t = parse_iso8601(json_object_string(meta, "published"));

	to_iso8601(t, dbuf, state->tz_h, state->tz_m, 1);
	snprintf(buf, 64, "%s.datetime_human", prefix);
	check_nerr(hdf_set_value(hdf, buf, dbuf));

	to_iso8601(t, dbuf, 0, 0, 0);
	snprintf(buf, 64, "%s.datetime", prefix);
	check_nerr(hdf_set_value(hdf, buf, dbuf));

	snprintf(buf, 64, "%s.type", prefix);
	check_nerr(hdf_set_value(hdf, buf, json_object_string(row, "type")));

	if (strcmp(json_object_string(row, "type"), "Announce") == 0) {
		json_t *obj = ds_select_one(
			datastores[DS_OBJECTS],
			CNDL(ISEQ_STR(id, json_object_string(row, "object"))));
		db_check_error(1);
		if (obj == NULL) {
			return 0;
		}

		HDF *rhdf;
		check_nerr(hdf_init(&rhdf));
		check_nerr(hdf_copy(rhdf, "", hdf));
		do_note(rhdf, state, "item", obj, 0, hide_private);
		json_decref(obj);
		char *sbuf;
		render_template_buf(&sbuf, rhdf, "templates/_note.html", 0);
		hdf_destroy(&rhdf);
		snprintf(buf, 64, "%s.sub", prefix);
		check_nerr(hdf_set_value(hdf, buf, sbuf));
		sfree(sbuf);
		return 1;
	}

	snprintf(buf, 64, "%s.url", prefix);
	char *note = aprintf("https://%s/web/note?id=%s", server_name,
	                     json_object_string(meta, "id"));
	check_nerr(hdf_set_value(hdf, buf, note));
	sfree(note);

	snprintf(buf, 64, "%s.content", prefix);
	check_nerr(hdf_set_value(hdf, buf, json_object_string(row, "content")));

	snprintf(buf, 64, "%s.id", prefix);
	check_nerr(hdf_set_value(hdf, buf, json_object_string(json_object_get(row, "$$meta"), "id")));
	const char *obj_id = json_object_string(row, "id");

	int likes = ds_count(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(type, "Like"),
		     ISEQ_STR(object, obj_id)));
	snprintf(buf, 64, "%s.like_num", prefix);
	char nbuf[64];
	snprintf(nbuf, 64, "%d", likes);
	check_nerr(hdf_set_value(hdf, buf, nbuf));

	int boosts = ds_count(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(type, "Announce"),
		     ISEQ_STR(object, obj_id)));
	snprintf(buf, 64, "%s.boost_num", prefix);
	snprintf(nbuf, 64, "%d", boosts);
	check_nerr(hdf_set_value(hdf, buf, nbuf));

	if (strstr(obj_id, server_name) == NULL) {
		snprintf(buf, 64, "%s.ext", prefix);
		check_nerr(hdf_set_value(hdf, buf, obj_id));
	}

	if (json_is_string(json_object_get(row, "inReplyTo")) && show_replies == 1) {
		json_t *parent = ds_select_one(
			datastores[DS_OBJECTS],
			CNDL(ISEQ_STR(id, json_object_string(row, "inReplyTo"))));
		if (parent != NULL) {
			snprintf(buf, 64, "%s.parent", prefix);
			char *sbuf = aprintf("https://%s/web/note?id=%s",
			                     server_name,
			                     json_object_string(
				                     json_object_get(parent, "$$meta"),
				                     "id"));
			check_nerr(hdf_set_value(hdf, buf, sbuf));
			sfree(sbuf);
			json_incref(parent);
			json_t *o = parent;
			for (;;) {
				json_t *p = json_object_get(parent, "inReplyTo");
				if (p == NULL || json_is_null(p)) {
					break;
				}
				const char *parent_id = json_string_value(p);
				json_t *obj = ds_select_one(
					datastores[DS_OBJECTS],
					CNDL(ISEQ_STR(id, parent_id)));
				json_decref(parent);
				parent = obj;
			}
			if (parent != o) {
				snprintf(buf, 64, "%s.top", prefix);
				sbuf = aprintf("https://%s/web/note?id=%s",
				               server_name,
				               json_object_string(
					               json_object_get(parent, "$$meta"),
					               "id"));
				check_nerr(hdf_set_value(hdf, buf, sbuf));
				sfree(sbuf);
			}
			json_decref(parent);
			json_decref(o);
		}
	}

	if (show_replies) {
		cur_t *replies = ds_select_ordered(
			datastores[DS_OBJECTS],
			CNDL(ISEQ_STR(inReplyTo, obj_id)),
			"$$meta.inserted", 0, -1);
		int nrows = cur_nrows(replies);
		if (nrows) {
			snprintf(buf, 64, "%s.has_replies", prefix);
			check_nerr(hdf_set_value(hdf, buf, "1"));
		}
		for (int i = 0; i < nrows; i++) {
			HDF *rhdf;
			check_nerr(hdf_init(&rhdf));
			check_nerr(hdf_copy(rhdf, "", hdf));
			snprintf(buf, 64, "%s.replies", prefix);
			check_nerr(hdf_remove_tree(rhdf, buf));
			json_t *obj = cur_get(replies, i);
			do_note(rhdf, state, "item", obj, 2, hide_private);
			json_decref(obj);
			char *sbuf;
			render_template_buf(&sbuf, rhdf, "templates/_note.html", 0);
			hdf_destroy(&rhdf);
			snprintf(buf, 64, "%s.replies.%d", prefix, i);
			check_nerr(hdf_set_value(hdf, buf, sbuf));
			sfree(sbuf);
		}
		cur_close(replies);
	}

	return 1;
}

#define do_timeline(a, b, c, d, e, f, g) do_timeline_raw(a, b, c, d, e, f, g, 0)
#define do_notifications(a, b, c, d, e, f, g) do_timeline_raw(a, b, c, d, e, f, g, 1)

static void do_timeline_raw(FCGX_Request *req, HDF *hdf, festate_t *state, const char ***cnd,
                            const char *title, char *template, char *path_base,
                            char do_notifications) {
	char hide_private = !do_notifications;
	char ***cndl;
	if (cnd == NULL) {
		cndl = salloc(sizeof(char**) * 2);
	} else {
		int i = 0;
		while (cnd[i++] != NULL) {}
		cndl = salloc(sizeof(char**) * (i + 1));
		memcpy(&cndl[1], cnd, sizeof(char**) * (i - 1));
	}

	char *before = hdf_get_value(hdf, "req.query.before", "99999999999999999999999");
	if (do_notifications) {
		cndl[0] = (char**)IS_LT(inserted, before);
	} else {
		cndl[0] = (char**)IS_LT($$meta.inserted, before);
	}

	cur_t *cur;

	if (do_notifications) {
		check_nerr(hdf_set_value(hdf, "timeline_template", "templates/_notification.html"));
		cur = ds_select_ordered(
			datastores[DS_NOTIFICATIONS], (const char***)cndl,
			"inserted", 1, 50);
	} else {
		check_nerr(hdf_set_value(hdf, "timeline_template", "templates/_note.html"));
		cur = ds_select_ordered(
			datastores[DS_OBJECTS], (const char***)cndl,
			"$$meta.inserted", 1, 50);
	}
	db_check_error(1);

	char *first_ins = NULL;
	char *last_ins = NULL;
	// insert the notes in the HDF.
	int nrows = cur_nrows(cur);
	int n_i = 0;
	for (int i = 0; i < nrows; i++) {
		char prefix[64];
		json_t *row = cur_get(cur, i);

		snprintf(prefix, 64, "notes.%d", n_i);

		sfree(last_ins);
		if (do_notifications) {
			if (first_ins == NULL) {
				first_ins = sstrdup(json_object_string(row, "inserted"));
			}
			last_ins = sstrdup(json_object_string(row, "inserted"));

			const char *object = json_object_string(row, "object");

			char buf[128];
			snprintf(buf, 128, "%s.notif.type", prefix);
			check_nerr(hdf_set_value(hdf, buf, json_object_string(row, "type")));
			snprintf(buf, 128, "%s.notif.read", prefix);
			check_nerr(hdf_set_value(hdf, buf, json_object_string(row, "read")));

			char *name, *url;
			get_actor_data(json_object_string(row, "source"), &name, &url);
			snprintf(buf, 128, "%s.notif.source.name", prefix);
			check_nerr(hdf_set_value(hdf, buf, name));
			snprintf(buf, 128, "%s.notif.source.url", prefix);
			check_nerr(hdf_set_value(hdf, buf, url));
			sfree(name);
			sfree(url);

			if (object == NULL) {
				json_decref(row);
				n_i++;
				continue;
			}
			json_t *note = ds_select_one(
				datastores[DS_OBJECTS],
				CNDL(ISEQ_STR(id, json_object_string(row, "object"))));
			json_decref(row);
			row = note;
		} else {
			if (first_ins == NULL) {
				first_ins = sstrdup(
					json_object_string(json_object_get(row, "$$meta"), "inserted"));
			}
			last_ins = sstrdup(
				json_string_value(
					json_object_get(json_object_get(row, "$$meta"), "inserted")));
		}

		if (do_note(hdf, state, prefix, row, 0, hide_private)) {
			n_i++;
		}

		json_decref(row);
	}

	cur_close(cur);
	db_check_error(1);

	if (last_ins != NULL) {
		cndl[0][2] = last_ins;
		json_t *obj = ds_select_one(
			datastores[do_notifications ? DS_NOTIFICATIONS : DS_OBJECTS],
			(const char***)cndl);
		db_check_error(1);
		if (obj != NULL) {
			check_nerr(hdf_set_value(hdf, "last_inserted", last_ins));
			json_decref(obj);
		}
	}
	if (first_ins != NULL) {
		check_nerr(hdf_set_value(hdf, "first_inserted", first_ins));
	}

	check_nerr(hdf_set_value(hdf, "path_base", path_base));
	set_title(title);
	FCGX_FPrintF(req->out, "Content-Type: text/html\r\n\r\n");
	render_template(req, hdf, template);
	sfree(first_ins);
	sfree(last_ins);
	sfree(cndl);
}

static int webfe_login(FCGX_Request *req, HDF *hdf, festate_t *state) {
	// get form parameters.
	char *username = hdf_get_value(hdf, "req.form.username", NULL);
	char *password = hdf_get_value(hdf, "req.form.password", NULL);

	if (username == NULL || password == NULL) {
		return 0;
	}

	// try to login.
	json_t *obj = auth_login(username, password);
	if (obj == NULL) {
		return 2;
	}

	// create the auth cookie.
	char cookie[66];
	auth_cookie_create(cookie, obj);
	json_decref(obj);

	// set the cookie.
	cookie_set_session(req, "auth", cookie, "/");

	return 1;
}

static int webfe_create_account(FCGX_Request *req, HDF *hdf, festate_t *state) {
	// get form parameters.
	char *username = hdf_get_value(hdf, "req.form.username", NULL);
	char *password = hdf_get_value(hdf, "req.form.password", NULL);
	char *confirm_password = hdf_get_value(hdf, "req.form.confirm-password", NULL);
	if (username == NULL ||
	    password == NULL ||
	    confirm_password == NULL) {
		return 0;
	}

	// check password & confirm password are equal.
	if (strcmp(password, confirm_password) != 0) {
		return 4;
	}

	switch (iapi_create_account(username, password)) {
	case IAPI_SUCCESS:
		return 1;
	case IAPI_ACCOUNT_INVALID_NAME:
		return 2;
	case IAPI_ACCOUNT_EMPTY_PASSWORD:
		return 3;
	case IAPI_ACCOUNT_EXISTS:
		return 5;
	default:
		return 0;
	}
}

static int webfe_submit(FCGX_Request *req, HDF *hdf, festate_t *state) {
	// get form parameters.
	char *content = hdf_get_value(hdf, "req.form.content", NULL);
	if (content == NULL) {
		return 0;
	}
	char *reply = hdf_get_value(hdf, "req.query.reply", NULL);

	switch (iapi_submit_note(state->actor, reply, content)) {
	case IAPI_SUCCESS:
		return 1;
	case IAPI_SUBMIT_EMPTY_CONTENT:
		return 2;
	case IAPI_SUBMIT_LONG_CONTENT:
		return 3;
	default:
		return 0;
	}
}

void webfe_handle(FCGX_Request *req, HDF *hdf) {
	char *path = hdf_get_value(hdf, "req.path", "/");
	char *method = hdf_get_value(hdf, "req.method", "GET");
	char is_post = strcmp(method, "POST") == 0;

	// parse timezone cookies.
	int tzh = atoi(hdf_get_value(hdf, "req.cookies.tzh", "0"));
	if (tzh < -12 || tzh > 12) {
		tzh = 0;
	}

	int tzm = atoi(hdf_get_value(hdf, "req.cookies.tzm", "0"));
	if (tzm != 0 && tzm != 15 && tzm != 30 && tzm != 45) {
		tzm = 0;
	}

	// parse locale cookie.
	char *locale = hdf_get_value(hdf, "req.cookies.lang", "en");
	if (hdf_get_obj(langs, locale) == NULL) {
		locale = "en";
	}

	// parse auth cookie and try to log in.
	char *auth;
	json_t *auth_actor = NULL;
	if ((auth = hdf_get_value(hdf, "req.cookies.auth", NULL)) != NULL) {
		if ((auth_actor = auth_cookie_verify(auth)) == NULL) {
			auth = NULL;
		}
	}

	// populate user-related HDF values.
	if (auth_actor != NULL) {
		check_nerr(hdf_set_value(hdf, "user.actor_id",
		                         json_object_string(auth_actor, "id")));
		check_nerr(hdf_set_value(hdf, "user.username",
		                         json_object_string(auth_actor, "preferredUsername")));
		check_nerr(hdf_set_value(hdf, "user.url",
		                         json_object_string(auth_actor, "url")));
	}

	// populate state.
	festate_t _state = {
		.tz_h = tzh,
		.tz_m = tzm,
		.locale = locale,
		.actor = auth_actor,
	};
	festate_t *state = &_state;

	// add some git info.
	check_nerr(hdf_set_value(hdf, "git.refspec", GIT_REFSPEC));
	check_nerr(hdf_set_value(hdf, "git.sha1", GIT_SHA1));

	// find the number of notifications.
	char nbuf[64];
	int notif_count = 0;
	if (state->actor != NULL) {
		notif_count = ds_count(
			datastores[DS_NOTIFICATIONS],
			CNDL(ISEQ_STR(actor, json_object_string(state->actor, "id")),
			     ISEQ_STR(read, "n")));
	}
	snprintf(nbuf, 64, "%d", notif_count);
	check_nerr(hdf_set_value(hdf, "notif_count", nbuf));

	if (strcmp(path, "/web") == 0) {
		set_title(get_title(index));
		FCGX_FPrintF(req->out, "Content-Type: text/html\r\n\r\n");
		render_template(req, hdf, "templates/index.html");
	} else if (strcmp(path, "/web/settings") == 0) {
		i18n_fill_meta(hdf, "langs");

		// populate HDF.
		snprintf(nbuf, 64, "%d", state->tz_h);
		check_nerr(hdf_set_value(hdf, "tzh", nbuf));

		snprintf(nbuf, 64, "%d", state->tz_m);
		check_nerr(hdf_set_value(hdf, "tzm", nbuf));

		check_nerr(hdf_set_value(hdf, "locale", state->locale));

		form_error(req, hdf, state, is_post, "settings", webfe_settings,
		           "templates/settings.html", get_title(settings));
	} else if (strcmp(path, "/web/create-account") == 0) {
		form_error(req, hdf, state, is_post, "create-account", webfe_create_account,
		           "templates/create-account.html", get_title(create_account));
	} else if (strcmp(path, "/web/login") == 0) {
		form_error(req, hdf, state, is_post, "login", webfe_login,
		           "templates/login.html", get_title(login));
	} else if (strcmp(path, "/web/logout") == 0) {
		cookie_delete(req, "auth", "/");
		redirect(req, "303 See Other", "/");
	} else if (strcmp(path, "/web/public-timeline") == 0) {
		do_timeline(req, hdf, state, NULL, get_title(public_timeline),
		            "templates/public-timeline.html", "/web/public-timeline");
	} else if (strcmp(path, "/web/note") == 0) {
		char *id = hdf_get_value(hdf, "req.query.id", "");
		json_t *obj = ds_select_one(datastores[DS_OBJECTS],
		                            CNDL(ISEQ_STR($$meta.id, id)));
		db_check_error(1);

		if (obj == NULL) {
			http_error(req, "404 Not Found");
			goto webfe_end;
		}

		do_note(hdf, state, "item", obj, 1, 1);
		json_decref(obj);

		set_title(get_title(note));
		FCGX_FPrintF(req->out, "Content-Type: text/html\r\n\r\n");
		render_template(req, hdf, "templates/note.html");
	} else if (strcmp(path, "/web/notifications") == 0) {
		if (state->actor == NULL) {
			http_error(req, "403 Forbidden");
			goto webfe_end;
		}

		do_notifications(req, hdf, state,
		                 CNDL(ISEQ_STR(actor, json_object_string(state->actor, "id"))),
		                 get_title(notifications),
		                 "templates/notifications.html", "/web/notifications");
	} else if (strcmp(path, "/web/user") == 0) {
		char *username = hdf_get_value(hdf, "req.query.username", NULL);
		if (username == NULL) {
			http_error(req, "400 Bad Request");
			goto webfe_end;
		}

		// parse username and domain.
		char *at = strchr(username, '@');
		char *domain;
		if (at != NULL) {
			*at = 0;
			domain = at + 1;
		} else {
			domain = (char*)server_name;
		}

		// set computed username.
		char *buf = aprintf("@%s@%s", username, domain);
		check_nerr(hdf_set_value(hdf, "tuser.username", buf));

		// retrieve actor.
		json_t *actor = webfinger(username, domain);
		if (actor == NULL) {
			http_error(req, "404 Not Found");
			sfree(buf);
			goto webfe_end;
		}

		// get url.
		const char *url;
		if (json_object_get(actor, "url") != NULL) {
			url = json_object_string(actor, "url");
		} else {
			url = json_object_string(actor, "id");
		}
		check_nerr(hdf_set_value(hdf, "tuser.url", url));

		// get name.
		char *name;
		if (json_object_get(actor, "name") != NULL) {
			name = (char*)json_object_string(actor, "name");
		} else {
			name = buf;
		}
		check_nerr(hdf_set_value(hdf, "tuser.name", name));
		sfree(buf);

		// get followers and follows.
		json_t *follows = ds_select_one(
			datastores[DS_FOLLOWS],
			CNDL(ISEQ_STR(actor, json_object_string(actor, "id"))));
		db_check_error(1);
		int followers = 0, followings = 0;
		if (follows != NULL) {
			followers = json_array_size(json_object_get(follows, "followers"));
			followings = json_array_size(json_object_get(follows, "followings"));
			json_decref(follows);
		}
		snprintf(nbuf, 64, "%d", followers);
		check_nerr(hdf_set_value(hdf, "tuser.followers", nbuf));
		snprintf(nbuf, 64, "%d", followings);
		check_nerr(hdf_set_value(hdf, "tuser.followings", nbuf));

		// render the timeline.
		const char *author = json_object_string(actor, "id");
		char *base_url = aprintf("https://%s/web/user?username=%s",
		                         server_name, username);

		do_timeline(req, hdf, state, CNDL(ISEQ_STR(attributedTo, author)), get_title(user),
		            "templates/user.html", base_url);

		sfree(base_url);
		json_decref(actor);
	} else if (strcmp(path, "/web/submit") == 0) {
		if (state->actor == NULL) {
			http_error(req, "403 Forbidden");
			goto webfe_end;
		}

		char *reply = hdf_get_value(hdf, "req.query.reply", NULL);

		if (reply != NULL) {
			// if it's a reply: get the reply object.
			json_t *reply_obj = ds_select_one(
				datastores[DS_OBJECTS],
				CNDL(ISEQ_STR($$meta.id, reply)));

			if (reply_obj == NULL) {
				http_error(req, "404 Not Found");
				goto webfe_end;
			}

			// add mentions to the text.
			json_t *mentions = json_array();
#define insert_actor(aid)	  \
			{ \
				json_t *t = ds_select_one( \
					datastores[DS_ACTORS], \
					CNDL(ISEQ_STR(id, aid))); \
				if (t != NULL) { \
					const char *domain = json_object_string(json_object_get(t, "$$meta"), \
					                                        "domain"); \
					const char *username = json_object_string(t, "preferredUsername"); \
					char *s; \
					if (strcmp(domain, server_name) == 0) { \
						s = aprintf("@%s ", username); \
					} else { \
						s = aprintf("@%s@%s ", username, domain); \
					} \
					json_t *o; \
					size_t j; \
					char ok = 1; \
					json_array_foreach(mentions, j, o) { \
						if (strcmp(json_string_value(o), s) == 0) { \
							ok = 0; \
							break; \
						} \
					} \
					if (ok) { \
						content = srealloc(content, content_l + strlen(s) + 1); \
						strcpy(content + content_l, s); \
						content_l += strlen(s); \
						json_array_append_new(mentions, json_string(s)); \
					} \
					json_decref(t); \
					sfree(s); \
				} \
			}
			size_t content_l = 0;
			char *content = NULL;
			// add author of parent post.
			insert_actor(json_object_string(reply_obj, "attributedTo"));
			json_t *obj;
			size_t i;
			json_t *tag = json_object_get(reply_obj, "tag");
			// add everyone tagged.
			if (tag != NULL && json_is_array(tag)) {
				json_array_foreach(tag, i, obj) {
					if (!json_is_object(obj)) {
						continue;
					}
					json_t *type = json_object_get(obj, "type");
					json_t *href = json_object_get(obj, "href");
					if (type != NULL && href != NULL &&
					    json_is_string(type) && json_is_string(href) &&
					    strcmp(json_string_value(type), "Mention") == 0 &&
					    strcmp(json_string_value(href),
					           json_object_string(state->actor, "id")) != 0) {
						insert_actor(json_string_value(href));
					}
				}
			}
			json_decref(mentions);
			check_nerr(hdf_set_value(hdf, "content", content));
			sfree(content);
			do_note(hdf, state, "item", reply_obj, 0, 1);
			json_decref(reply_obj);
		}

		char *base_url;
		if (reply == NULL) {
			base_url = aprintf("submit");
		} else {
			base_url = aprintf("submit?reply=%s", reply);
		}

		check_nerr(hdf_set_value(hdf, "base_url", base_url));
		form_error(req, hdf, state, is_post, base_url, webfe_submit,
		           "templates/submit.html", get_title(submit));
		sfree(base_url);
	} else if (strcmp(path, "/web/like") == 0) {
		if (state->actor == NULL) {
			http_error(req, "403 Forbidden");
			goto webfe_end;
		}
		char *id = hdf_get_value(hdf, "req.query.id", NULL);
		if (id == NULL) {
			http_error(req, "400 Bad Request");
			goto webfe_end;
		}

		// find object.
		json_t *object = ds_select_one(
			datastores[DS_OBJECTS],
			CNDL(ISNTEQ_STR(type, "Announce"),
			     ISEQ_STR($$meta.id, id)));
		db_check_error(1);
		if (object == NULL) {
			http_error(req, "404 Not Found");
			goto webfe_end;
		}

		iapi_object_like(state->actor, object);
		json_decref(object);

		char *ref = hdf_get_value(hdf, "req.headers.referer", "/web");
		redirect(req, "303 See Other", ref);
	} else if (strcmp(path, "/web/boost") == 0) {
		if (state->actor == NULL) {
			http_error(req, "403 Forbidden");
			goto webfe_end;
		}
		char *id = hdf_get_value(hdf, "req.query.id", NULL);
		if (id == NULL) {
			http_error(req, "400 Bad Request");
			goto webfe_end;
		}

		// find object.
		json_t *object = ds_select_one(
			datastores[DS_OBJECTS],
			CNDL(ISNTEQ_STR(type, "Announce"),
			     ISEQ_STR($$meta.id, id)));
		db_check_error(1);
		if (object == NULL) {
			http_error(req, "404 Not Found");
			goto webfe_end;
		}

		iapi_object_boost(state->actor, object);
		json_decref(object);

		char *ref = hdf_get_value(hdf, "req.headers.referer", "/web");
		redirect(req, "303 See Other", ref);
	} else if (strcmp(path, "/web/readnotifications") == 0) {
		if (state->actor == NULL) {
			http_error(req, "403 Forbidden");
			goto webfe_end;
		}
		char *from = hdf_get_value(hdf, "req.query.from", NULL);
		if (from == NULL) {
			http_error(req, "400 Bad Request");
			goto webfe_end;
		}

		// get all notifications.
		cur_t *cur = ds_select(
			datastores[DS_NOTIFICATIONS],
			CNDL(ISEQ_STR(actor, json_object_string(state->actor, "id")),
			     ISEQ_STR(read, "n"),
			     IS_LE(inserted, from)));
		db_check_error(1);
		int nrows = cur_nrows(cur);
		db_check_error(1);

		for (int i = 0; i < nrows; i++) {
			// set "read" on each one.
			json_t *obj = cur_get(cur, i);
			db_check_error(1);

			json_object_del(obj, "read");
			json_object_set_new(obj, "read", json_string("y"));

			transaction_t *trans = trans_init(dbc);
			db_check_error(1);

			ds_update(
				datastores[DS_NOTIFICATIONS], &trans,
				CNDL(ISEQ_STR(id, json_object_string(obj, "id"))),
				obj);
			db_check_error(1);

			trans_commit(trans);
			db_check_error(1);

			json_decref(obj);
		}


		cur_close(cur);
		db_check_error(1);

		redirect(req, "303 See Other", "/web/notifications");
	} else {
		http_error(req, "404 Not Found");
	}

webfe_end:
	// cleanup.
	if (state->actor != NULL) {
		json_decref(state->actor);
	}
}
