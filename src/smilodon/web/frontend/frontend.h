/**
 * @file
 * @brief Smilodon web frontend.
 */

#pragma once

#include <fcgiapp.h>
#include <jansson.h>
#include <util/neo_hdf.h>

/// @brief Shared state for frontent endpoints.
typedef struct frontend_state {
	/// @brief Timezone hour offset.
	int tz_h;

	/// @brief Timezone minute offset.
	int tz_m;

	/// @brief Frontend language.
	const char *locale;

	/// @brief Logged in user.
	json_t *actor;
} festate_t;

/**
 * @brief Account-related and general settings.
 * @param req FastCGI request.
 * @param hdf HDF.
 * @param state Frontend state.
 * @return 1 if successful, 0 for a 400, else a number containing all the errors.
 */
int webfe_settings(FCGX_Request *req, HDF *hdf, festate_t *state);

/**
 * @brief Handle a web frontend request.
 * @param req FastCGI request.
 * @param hdf HDF.
 */
void webfe_handle(FCGX_Request *req, HDF *hdf);
