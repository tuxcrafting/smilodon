#include "smilodon/web/frontend/frontend.h"

#include <fcgiapp.h>
#include <stddef.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "smilodon/i18n.h"
#include "smilodon/web/server.h"

int webfe_settings(FCGX_Request *req, HDF *hdf, festate_t *state) {
	// get form parameters.
	char *lang = hdf_get_value(hdf, "req.form.lang", NULL);
	char *tz_hour = hdf_get_value(hdf, "req.form.tz_hour", NULL);
	char *tz_min = hdf_get_value(hdf, "req.form.tz_min", NULL);
	if (lang == NULL || tz_hour == NULL || tz_min == NULL) {
		return 0;
	}

	// check selected locale exists.
	if (hdf_get_obj(langs, lang) == NULL) {
		return 0;
	}

	// parse timezone.
	int tzh = atoi(tz_hour);
	int tzm = atoi(tz_min);
	if (tzh < -12 || tzh > 12) {
		return 2;
	}
	if (tzm != 0 && tzm != 15 && tzm != 30 && tzm != 45) {
		return 2;
	}

	// set cookies.
	cookie_set_persistent(req, "lang", lang, "/");
	char nbuf[64];
	snprintf(nbuf, 64, "%d", tzh);
	cookie_set_persistent(req, "tzh", nbuf, "/");
	snprintf(nbuf, 64, "%d", tzm);
	cookie_set_persistent(req, "tzm", nbuf, "/");

	return 1;
}
