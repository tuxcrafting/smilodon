#include "smilodon/web/nodeinfo.h"

#include <fcgiapp.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "build_config.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"
#include "smilodon/web/template.h"

void nodeinfo_handle(FCGX_Request *req, HDF *hdf) {
	if (strcmp(hdf_get_value(hdf, "req.path", "/"), "/nodeinfo/2.0.json") == 0) {
		check_nerr(hdf_set_value(hdf, "version", GIT_REFSPEC " " GIT_SHA1));
		char buf[64];
		int posts = 0;
		cur_t *cur = ds_select(datastores[DS_USERS], COND_TRUE);
		int nrows = cur_nrows(cur);
		snprintf(buf, 64, "%d", nrows);
		for (int i = 0; i < nrows; i++) {
			json_t *o = cur_get(cur, i);
			posts += ds_count(
				datastores[DS_OBJECTS],
				CNDL(ISEQ_STR(attributedTo, json_object_string(o, "actor"))));
			json_decref(o);
		}
		cur_close(cur);
		check_nerr(hdf_set_value(hdf, "users", buf));
		snprintf(buf, 64, "%d", posts);
		check_nerr(hdf_set_value(hdf, "posts", buf));
		FCGX_FPrintF(req->out,
		             "Content-Type: "
		             "application/json; "
		             "profile=http://nodeinfo.diaspora.software/ns/schema/2.0#\r\n\r\n");
		render_template(req, hdf, "templates/nodeinfo-2.0.json");
	} else {
		http_error(req, "404 Not Found");
	}
}
