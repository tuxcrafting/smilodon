/**
 * @file
 * @brief Threaded FastCGI server.
 */

#pragma once

#include <curl/curl.h>
#include <fcgiapp.h>
#include <pthread.h>
#include <time.h>

/// @brief The server name loaded from the configuration.
extern const char *server_name;

/**
 * @brief Enqueue a callback and notify the event loop.
 * @param function Function.
 * @param a First parameter.
 * @param b Second parameter.
 * @param thread Calling thread (will be joined).
 * @return 1 if successful, 0 on error.
 */
char enqueue_callback(void (*function)(void*, void*), void *a, void *b, pthread_t thread);

/**
 * @brief Start the server.
 * @param path Socket path.
 * @param worker_n Number of worker threads.
 * @param backlog Socket backlog.
 */
void start_server(const char *path, int worker_n, int backlog);

/**
 * @brief Stop the server.
 */
void stop_server();

/**
 * @brief Send a HTTP redirect.
 * @param req FastCGI request.
 * @param status HTTP status.
 * @param location Target location.
 */
void redirect(FCGX_Request *req, const char *status, const char *location);

/**
 * @brief Send a HTTP error page.
 * @param req FastCGI request.
 * @param status HTTP status.
 */
void http_error(FCGX_Request *req, const char *status);

/// @brief Persistent cookie expiration date.
#define COOKIE_PERSISTENT_EXP 2147483646

/**
 * @brief Set a cookie.
 * @param req FastCGI request.
 * @param name Cookie name.
 * @param value Cookie value.
 * @param path Cookie path.
 * @param expiration Expiration date. (time_t)-1 is for a session cookie.
 */
void cookie_set(FCGX_Request *req, const char *name, const char *value,
                const char *path, time_t expiration);

/**
 * @brief Set a session cookie.
 * @param req FastCGI request.
 * @param name Cookie name.
 * @param value Cookie value.
 * @param path Cookie path.
 */
#define cookie_set_session(req, name, value, path) cookie_set(req, name, value, path, (time_t)-1)

/**
 * @brief Set a persistent cookie.
 * @param req FastCGI request.
 * @param name Cookie name.
 * @param value Cookie value.
 * @param path Cookie path.
 */
#define cookie_set_persistent(req, name, value, path) cookie_set(req, name, value, path, COOKIE_PERSISTENT_EXP)

/**
 * @brief Delete a cookie.
 * @param req FastCGI request.
 * @param name Cookie name.
 * @param path Cookie path.
 */
#define cookie_delete(req, name, path) cookie_set(req, name, "delete", path, 0)
