#include "smilodon/web/well_known.h"

#include <fcgiapp.h>
#include <jansson.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"
#include "smilodon/web/template.h"

// handle a webfinger request.
static void do_webfinger(FCGX_Request *req, HDF *hdf) {
	// ensure there is a resource parameter.
	char *resource = hdf_get_value(hdf, "req.query.resource", NULL);
	if (resource == NULL) {
		http_error(req, "400 Bad Request");
		return;
	}

	char *domain;

	// check it starts with acct: and there is an @
	if (strstr(resource, "acct:") != resource ||
	    (domain = strchr(resource, '@')) == NULL) {
		http_error(req, "404 Not Found");
		return;
	}

	// check that the domain part is the server name.
	if (strcmp(domain + 1, server_name) != 0) {
		http_error(req, "404 Not Found");
		return;
	}

	// get the user entry.
	char *username = resource + 5;
	char *buf = salloc(domain - username + 1);
	memcpy(buf, username, domain - username);

	json_t *user = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR(username, buf)));
	db_check_error(1);
	sfree(buf);

	if (user == NULL) {
		http_error(req, "404 Not Found");
		return;
	}

	// render the template.
	hdf_set_value(hdf, "resource", resource);
	hdf_set_value(hdf, "user.id", json_object_string(user, "actor"));
	FCGX_FPrintF(req->out, "Content-Type: application/json\r\n\r\n");
	render_template(req, hdf, "templates/webfinger.json");

	json_decref(user);
}

void wellknown_handle(FCGX_Request *req, HDF *hdf) {
	char *path = hdf_get_value(hdf, "req.path", "/");

	if (strcmp(path, "/.well-known/host-meta") == 0) {
		// host meta.
		hdf_set_value(hdf, "server_name", server_name);
		FCGX_FPrintF(req->out, "Content-Type: application/xrd+xml\r\n\r\n");
		render_template(req, hdf, "templates/host-meta.xml");
	} else if (strcmp(path, "/.well-known/webfinger") == 0) {
		do_webfinger(req, hdf);
	} else if (strcmp(path, "/.well-known/nodeinfo") == 0) {
		// nodeinfo.
		hdf_set_value(hdf, "server_name", server_name);
		FCGX_FPrintF(req->out, "Content-Type: application/json\r\n\r\n");
		render_template(req, hdf, "templates/nodeinfo.json");
	} else {
		http_error(req, "404 Not Found");
	}
}
