#include "smilodon/web/webfinger.h"

#include <jansson.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdint.h>
#include <string.h>

#include "smilodon/activitypub/actor.h"
#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"

static json_t *get_instance(const char *domain) {
	// try to get it from the DB first.
	json_t *instance = ds_select_one(
		datastores[DS_INSTANCES],
		CNDL(ISEQ_STR(domain, domain)));
	db_check_error(1);
	if (instance != NULL) {
		return instance;
	}

	// retrieve the host meta.
	char *url = aprintf("https://%s/.well-known/host-meta", domain);
	char *res = curlw_req_sync(url, NULL, (const char*[]){ "Accept: application/xml", NULL });
	sfree(url);
	if (res == NULL) {
		return NULL;
	}
	xmlDocPtr doc = xmlParseMemory(res, strlen(res));
	sfree(res);
	if (doc == NULL) {
		return NULL;
	}

	// find lrdd node.
	xmlNodePtr root = xmlDocGetRootElement(doc);
	if (root == NULL || strcmp((char*)root->name, "XRD") != 0) {
		xmlFreeDoc(doc);
		return NULL;
	}
	xmlNodePtr node = root->children;
	while (node != NULL) {
		if (strcmp((char*)node->name, "Link") == 0) {
			char *rel = (char*)xmlGetProp(node, (xmlChar*)"rel");
			char *template = (char*)xmlGetProp(node, (xmlChar*)"template");
			if (rel != NULL && template != NULL &&
			    strcmp(rel, "lrdd") == 0) {
				// create the instance object.
				instance = json_pack(
					"{ssss}",
					"domain", domain,
					"webfinger", template);

				// insert it.
				transaction_t *trans = trans_init(dbc);
				db_check_error(1);
				ds_insert(datastores[DS_INSTANCES], &trans, instance);
				db_check_error(1);
				trans_commit(trans);
				db_check_error(1);

				xmlFreeDoc(doc);
				sfree(rel);
				sfree(template);
				return instance;
			}
			sfree(rel);
			sfree(template);
		}
		node = node->next;
	}

	xmlFreeDoc(doc);
	return NULL;
}

json_t *webfinger(const char *username, const char *domain) {
	// try to retrieve the actor from the local DB first.
	json_t *actor = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(preferredUsername, username),
		     ISEQ_STR($$meta.domain, domain)));
	if (actor != NULL) {
		return actor;
	}

	// not found and local? it doesn't exist.
	if (strcmp(domain, server_name) == 0) {
		return NULL;
	}

	// get the webfinger URL.
	json_t *instance = get_instance(domain);

	// do a request.
	char *url;
	if (instance != NULL) {
		const char *tmp = json_object_string(instance, "webfinger");
		size_t l;
		char *p = strstr(tmp, "{uri}");
		if (p == NULL) {
			l = strlen(tmp);
		} else {
			l = p - tmp;
		}
		url = salloc(l + strlen(username) + strlen(domain) + 2);
		memcpy(url, tmp, l);
		json_decref(instance);
	} else {
#define _f "https://%s/.well-known/webfinger?resource=acct:"
		url = salloc(sizeof(_f) - 1 + strlen(username) + strlen(domain) * 2);
		sprintf(url, _f, domain);
	}
	size_t i = strlen(url);
	sprintf(url + i, "%s@%s", username, domain);
	char *res = curlw_req_sync(url, NULL, (const char*[]){ "Accept: application/json", NULL });
	sfree(url);
	if (res == NULL) {
		return NULL;
	}
	json_t *obj = json_loads(res, 0, NULL);
	sfree(res);
	if (obj == NULL) {
		return NULL;
	}

	// list the links to find the actor ID.
	json_t *links = json_object_get(obj, "links");
	if (links == NULL) {
		json_decref(obj);
		return NULL;
	}

	json_t *v;
	json_array_foreach(links, i, v) {
		json_t *rel, *type, *href;
		if (json_is_object(v) &&
		    json_is_string(rel = json_object_get(v, "rel")) &&
		    strcmp(json_string_value(rel), "self") == 0 &&
		    json_is_string(type = json_object_get(v, "type")) &&
		    strcmp(json_string_value(type), "application/activity+json") == 0 &&
		    json_is_string(href = json_object_get(v, "href"))) {
			// retrieve the actor.
			const char *href_v = json_string_value(href);
			actor = actor_get_sync(href_v, 1);
			json_decref(obj);
			return actor;
		}
	}

	json_decref(obj);
	return NULL;
}
