/**
 * @file
 * @brief Handle requests to /.well-known endpoints.
 */

#pragma once

#include <fcgiapp.h>
#include <util/neo_hdf.h>

/**
 * @brief Handle a request to a well-known endpoint.
 * @param req FastCGI request.
 * @param hdf HDF.
 */
void wellknown_handle(FCGX_Request *req, HDF *hdf);
