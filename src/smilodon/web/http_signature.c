#include "smilodon/web/http_signature.h"

#include <ctype.h>
#include <jansson.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <stdint.h>
#include <string.h>
#include <util/neo_hdf.h>

#include "smilodon/config.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"
#include "smilodon/web/template.h"

static char algorithm_rsa_sha256(const char *public_key, const char *data, const char *signature, void *ctx) {
	// create the message digest.
	uint8_t md[SHA256_DIGEST_LENGTH];
	SHA256((uint8_t*)data, strlen(data), md);

	// parse the public key.
	RSA *rsa = NULL;
	BIO *pubkey_bio = BIO_new_mem_buf(public_key, strlen(public_key));
	rsa = PEM_read_bio_RSA_PUBKEY(pubkey_bio, NULL, NULL, NULL);
	BIO_free(pubkey_bio);

	if (rsa == NULL) {
		return 0;
	}

	// dirty bit tricks to get the size the signature should have.
	size_t len = ((4 * RSA_size(rsa) / 3) + 3) & (size_t)~3;
	if (strlen(signature) != len) {
		RSA_free(rsa);
		return 0;
	}

	// decode the signature.
	uint8_t *sig = salloc(RSA_size(rsa) + 1);
	EVP_DecodeBlock(sig, (uint8_t*)signature, strlen(signature));

	int ver = RSA_verify(NID_sha256,
	                     md, SHA256_DIGEST_LENGTH,
	                     sig, RSA_size(rsa),
	                     rsa);

	sfree(sig);
	RSA_free(rsa);
	return ver;
}

// use gperf?
static char *algorithms[] = {
	"rsa-sha256", (char*)algorithm_rsa_sha256, NULL,
	NULL
};

char http_verify(HDF *hdf, const char *actor_id) {
	// get the header and parse it.
	char *sig_header = hdf_get_value(hdf, "req.headers.signature", NULL);
	if (sig_header == NULL) {
		return 0;
	}
	parse_parameters(hdf, sig_header, "signature", 3);

	// check all the fields exist.
#define hdf_get_value_fail(name)	  \
	char *name = hdf_get_value(hdf, "signature." #name, NULL); \
	if (name == NULL) { \
		return 0; \
	}

	hdf_get_value_fail(keyId);
	hdf_get_value_fail(algorithm);
	hdf_get_value_fail(headers);
	hdf_get_value_fail(signature);

	char *headers_ = headers;
	headers = sstrdup(headers_);

	// get the actor object by key ID and check the actor ID matches.
	json_t *actor = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(publicKey.id, keyId)));
	if (actor == NULL) {
		return 0;
	}

	if (actor_id != NULL &&
	    strcmp(json_string_value(
		           json_object_get(actor, "id")),
	           actor_id) != 0) {
		json_decref(actor);
		return 0;
	}

	HDF *headers_hdf = hdf_get_obj(hdf, "req.headers");

	char *request_target = NULL;

	// scan the headers to get the buffer length.
	size_t buf_len = 0;
	char *headers_saveptr;
	char *header;
	header = strtok_r(headers, " ", &headers_saveptr);
	do {
		buf_len += strlen(header) + 3;

		// handle (request-target) specially.
		if (strcmp(header, "(request-target)") == 0) {
			char *method = hdf_get_value(hdf, "req.method", NULL);
			char *path = hdf_get_value(hdf, "req.path", NULL);
			int len = strlen(method) + strlen(path) + 2;
			buf_len += len;

			if (request_target == NULL) {
				request_target = salloc(len);

				int i;
				for (i = 0; method[i] != 0; i++) {
					request_target[i] = tolower(method[i]);
				}
				request_target[i] = ' ';
				strcpy(request_target + i + 1, path);
			}
		} else {
			for (int i = 0; header[i] != 0; i++) {
				if (header[i] == '-') {
					header[i] = '_';
				}
			}

			char *header_data = hdf_get_value(headers_hdf, header, NULL);
			if (header_data == NULL) {
				sfree(headers);
				sfree(request_target);
				json_decref(actor);
				return 0;
			}

			buf_len += strlen(header_data);
		}
	} while ((header = strtok_r(NULL, " ", &headers_saveptr)) != NULL);

	sfree(headers);
	headers = strdup(headers_);

	// actually build the string buffer.
	char *buf = salloc(buf_len);
	header = strtok_r(headers, " ", &headers_saveptr);
	int i = 0;
	do {
		for (int j = 0; header[j] != 0; i++, j++) {
			buf[i] = header[j] == '_' ? '-' : header[j];
		}
		buf[i++] = ':';
		buf[i++] = ' ';

		if (strcmp(header, "(request-target)") == 0) {
			strcpy(buf + i, request_target);
			i += strlen(request_target);
		} else {
			for (int i = 0; header[i] != 0; i++) {
				if (header[i] == '-') {
					header[i] = '_';
				}
			}

			char *header_data = hdf_get_value(headers_hdf, header, NULL);
			strcpy(buf + i, header_data);
			i += strlen(header_data);
		}

		buf[i++] = '\n';
	} while ((header = strtok_r(NULL, " ", &headers_saveptr)) != NULL);

	buf[i - 1] = 0;
	sfree(headers);
	sfree(request_target);

	// get the algorithm function.
	char (*func)(const char*, const char*, const char*, void*) = NULL;
	void *ctx;
	for (int i = 0; algorithms[i] != NULL; i += 3) {
		if (strcmp(algorithms[i], algorithm) == 0) {
			func = (void*)algorithms[i + 1];
			ctx = algorithms[i + 2];
			break;
		}
	}

	if (func == NULL) {
		sfree(buf);
		json_decref(actor);
		return 0;
	}

	json_t *pkey = json_object_get(actor, "publicKey");
	json_t *pem = json_object_get(pkey, "publicKeyPem");

	char valid = func(json_string_value(pem), buf, signature, ctx);

	sfree(buf);
	json_decref(actor);

	return valid;
}

char **http_sign(const char *actor_id, const char *path, const char *body) {
	// retrieve the private key.
	json_t *user = ds_select_one(datastores[DS_USERS],
	                             CNDL(ISEQ_STR(actor, actor_id)));
	db_check_error(1);
	const char *private_key = json_object_string(user, "private_key");
	RSA *rsa = NULL;
	BIO *privkey_bio = BIO_new_mem_buf(private_key, strlen(private_key));
	rsa = PEM_read_bio_RSAPrivateKey(privkey_bio, NULL, NULL, (uint8_t*)secret);
	BIO_free(privkey_bio);
	json_decref(user);

	// retrieve the key ID.
	json_t *actor = ds_select_one(datastores[DS_ACTORS],
	                              CNDL(ISEQ_STR(id, actor_id)));
	db_check_error(1);
	const char *keyId = json_object_string(json_object_get(actor, "publicKey"), "id");

	path += 8;
	const char *c = strchrnul(path, '/');

	// create the signing payload.
	uint8_t digest[SHA256_DIGEST_LENGTH];
	SHA256((uint8_t*)body, strlen(body), digest);
	char *bdigest = salloc(1024);
	EVP_EncodeBlock((uint8_t*)bdigest, digest, SHA256_DIGEST_LENGTH);

	char *digest_header = aprintf("Digest: SHA-256=%s", bdigest);

	char *buf = aprintf(
		"(request-target): post %s\n"
		"host: %.*s\n"
		"digest: SHA-256=%s\n"
		"content-length: %zu",
		c, (int)(c - path), path,
		bdigest, strlen(body));

	SHA256((uint8_t*)buf, strlen(buf), digest);
	sfree(buf);

	// sign it.
	uint8_t *sigret = salloc(RSA_size(rsa));
	uint32_t siglen;
	RSA_sign(NID_sha256, digest, SHA256_DIGEST_LENGTH,
	         sigret, &siglen, rsa);
	EVP_EncodeBlock((uint8_t*)bdigest, sigret, siglen);
	char *signature_header = aprintf(
		"Signature: "
		"keyId=\"%s\","
		"algorithm=\"rsa-sha256\","
		"headers=\"(request-target) host digest content-length\","
		"signature=\"%s\"",
		keyId, bdigest);
	sfree(sigret);
	sfree(bdigest);

	RSA_free(rsa);
	json_decref(actor);

	// create the header list.
	char **headers = salloc(sizeof(char*) * 4);
	headers[0] = digest_header;
	headers[1] = signature_header;
	return headers;
}
