/**
 * @file
 * @brief Nodeinfo.
 */

#pragma once

#include <fcgiapp.h>
#include <util/neo_hdf.h>

/**
 * @brief Handle a nodeinfo request.
 * @param req FastCGI request.
 * @param hdf HDF.
 */
void nodeinfo_handle(FCGX_Request *req, HDF *hdf);
