/**
 * @file
 * @brief Inbox handler.
 */

#pragma once

#include <fcgiapp.h>
#include <jansson.h>
#include <util/neo_hdf.h>

/**
 * @brief Insert an activity.
 * @param activity Activity object.
 * @return 1 if successful, 0 otherwise.
 */
char inbox_handle_activity(json_t *activity);

/**
 * @brief Handle a request to the inbox or shared inbox.
 * @param req FastCGI request.
 * @param hdf HDF.
 */
void inbox_handle(FCGX_Request *req, HDF *hdf);
