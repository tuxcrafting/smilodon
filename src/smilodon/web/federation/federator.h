/**
 * @file
 * @brief Federator.
 */

#pragma once

#include <jansson.h>

/**
 * @brief Federate an activity.
 * @param activity Activity.
 */
void federate(json_t *activity);
