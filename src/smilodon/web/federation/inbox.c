#include "smilodon/web/federation/inbox.h"

#include <fcgiapp.h>
#include <inttypes.h>
#include <jansson.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <util/neo_hdf.h>

#include "smilodon/activitypub/actor.h"
#include "smilodon/activitypub/object.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/federation/federator.h"
#include "smilodon/web/http_signature.h"
#include "smilodon/web/server.h"

static void notify_object(const char *type, json_t *object, json_t *actor, json_t *source, json_t *inserted) {
	char buf[64];
	snprintf(buf, 64, "%" PRIu64, get_num_id());

	// create the notification.
	json_t *notification = json_pack(
		"{sssOsOsOssss}",
		"id", buf,
		"actor", actor,
		"source", source,
		"inserted", inserted,
		"read", "n",
		"type", type);
	if (object != NULL) {
		json_object_set(notification, "object", object);
	}

	// insert it.
	transaction_t *trans = trans_init(dbc);
	db_check_error(1);

	ds_insert(datastores[DS_NOTIFICATIONS], &trans, notification);
	db_check_error(1);

	trans_commit(trans);
	db_check_error(1);

	json_decref(notification);
}

static void _callback1(json_t *obj, void *data) {
	json_t *activity = data;
	if (obj == NULL) {
		json_decref(activity);
		return;
	}

	// insert notification.
	json_t *o = json_object_get(obj, "attributedTo");
	const char *d_actor = json_string_value(o);
	json_t *user = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR(actor, d_actor)));
	if (user != NULL) {
		json_decref(user);

		const char *name = json_object_string(activity, "type");
		if (strcmp(name, "Announce") == 0) {
			name = "Boost";
		}

		notify_object(name,
		              json_object_get(activity, "object"),
		              o,
		              json_object_get(activity, "actor"),
		              json_object_get(json_object_get(activity, "$$meta"), "inserted"));
	}

	json_decref(activity);
	json_decref(obj);
}

static void _callback2(json_t *obj, void *data) {
	if (obj != NULL) {
		json_decref(obj);
	}
}

static char handle_Create(transaction_t **trans, json_t *activity) {
	json_t *object = json_object_get(activity, "object");

	// populate $$meta.
	json_t *meta;
	json_object_set_new(
		object,
		"$$meta",
		(meta = json_pack(
			"{sosOsO}",
			"id", json_sprintf("%" PRIu64, get_num_id()),
			"published", json_object_get(activity, "published"),
			"inserted", json_object_get(
				json_object_get(activity, "$$meta"),
				"inserted"))));

	// reference the object ID in the activity.
	json_object_set(
		json_object_get(activity, "$$meta"),
		"object",
		json_object_get(meta, "id"));

	// try to insert the object in the transaction.
	ds_insert(datastores[DS_OBJECTS], trans, object);
	if (db_check_error(0)) {
		return 0;
	}

	// check it doesn't already exist.
	json_t *obj = ds_select_one(
		datastores[DS_OBJECTS],
		CNDL(ISEQ_STR(id, json_object_string(object, "id"))));
	db_check_error(1);
	if (obj != NULL) {
		json_decref(obj);
		return 0;
	}

	// try to fetch the whole thread.
	json_t *parent = json_object_get(object, "inReplyTo");
	if (parent != NULL && json_is_string(parent)) {
		object_get(json_string_value(parent), _callback2, NULL);
	}

	// insert notification.
	json_t *tag = json_object_get(object, "tag");
	if (tag != NULL && json_is_array(tag)) {
		json_t *o;
		size_t i;
		json_array_foreach(tag, i, o) {
			if (json_is_object(o)) {
				json_t *type = json_object_get(o, "type");
				json_t *href = json_object_get(o, "href");
				if (json_is_string(type) && json_is_string(href) &&
				    strcmp(json_string_value(type), "Mention") == 0) {
					const char *d_actor = json_string_value(href);
					json_t *user = ds_select_one(
						datastores[DS_USERS],
						CNDL(ISEQ_STR(actor, d_actor)));
					db_check_error(1);
					if (user != NULL) {
						json_decref(user);
						notify_object("Mention",
						              json_object_get(object, "id"),
						              href,
						              json_object_get(object, "attributedTo"),
						              json_object_get(json_object_get(object, "$$meta"),
						                              "inserted"));
					}
				}
			}
		}
	}

	return 1;
}

static char handle_Like(transaction_t **trans, json_t *activity) {
	// check there isn't already an activity with the same actor and object.
	json_t *obj = ds_select_one(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(type, "Like"),
		     ISEQ_STR(actor, json_object_string(activity, "actor")),
		     ISEQ_STR(object, json_object_string(activity, "object"))));
	db_check_error(1);
	if (obj != NULL) {
		json_decref(obj);
		return 0;
	}

	// try to fetch the object.
	json_incref(activity);
	object_get(json_object_string(activity, "object"), _callback1, activity);

	return 1;
}

static char handle_Announce(transaction_t **trans, json_t *activity) {
	// check there isn't already an activity with the same actor and object.
	json_t *obj = ds_select_one(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(type, "Announce"),
		     ISEQ_STR(actor, json_object_string(activity, "actor")),
		     ISEQ_STR(object, json_object_string(activity, "object"))));
	db_check_error(1);
	if (obj != NULL) {
		json_decref(obj);
		return 0;
	}

	// populate and insert pseudo-object.
	json_object_set(json_object_get(activity, "$$meta"), "published",
	                json_object_get(activity, "published"));
	json_object_set(activity, "attributedTo",
	                json_object_get(activity, "actor"));
	ds_insert(datastores[DS_OBJECTS], trans, activity);
	db_check_error(1);

	// try to fetch the object.
	json_incref(activity);
	object_get(json_object_string(activity, "object"), _callback1, activity);

	return 1;
}

static void _callback3(json_t *obj, void *data) {
	json_t *activity = data;
	if (obj == NULL) {
		json_decref(activity);
		return;
	}

	const char *actor = json_object_string(activity, "actor");

	transaction_t *trans = trans_init(dbc);
	db_check_error(1);

	// add the following.
	json_t *follows = ds_select_one(
		datastores[DS_FOLLOWS],
		CNDL(ISEQ_STR(actor, actor)));
	db_check_error(1);
	char insert = follows == NULL;
	if (follows == NULL) {
		follows = json_pack("{sss[]s[]}",
		                    "actor", actor,
		                    "followers", "followings");
	}
	json_array_append(json_object_get(follows, "followings"),
	                  json_object_get(activity, "object"));
	ds_update(
		datastores[DS_FOLLOWS],
		&trans,
		insert ? NULL : CNDL(ISEQ_STR(actor, actor)),
		follows);
	db_check_error(1);

	json_decref(follows);

	const char *object = json_object_string(activity, "object");
	// add the follower.
	follows = ds_select_one(
		datastores[DS_FOLLOWS],
		CNDL(ISEQ_STR(actor, object)));
	db_check_error(1);
	insert = follows == NULL;
	if (follows == NULL) {
		follows = json_pack("{sss[]s[]}",
		                    "actor", object,
		                    "followers", "followings");
	}
	json_array_append(json_object_get(follows, "followers"),
	                  json_object_get(activity, "actor"));
	ds_update(
		datastores[DS_FOLLOWS],
		&trans,
		insert ? NULL : CNDL(ISEQ_STR(actor, object)),
		follows);
	db_check_error(1);

	trans_commit(trans);
	db_check_error(1);

	json_decref(follows);

	// accept the follow.
	json_t *accept = json_pack(
		"{sosOsssOs[O]}",
		"id", json_sprintf("https://%s/accept.json?id=%" PRIu64, server_name, get_num_id()),
		"actor", json_object_get(activity, "object"),
		"type", "Accept",
		"object", json_object_get(activity, "id"),
		"to", json_object_get(activity, "actor"));
	int ret = inbox_handle_activity(accept);
	if (ret) {
		federate(accept);
	}
	json_decref(accept);

	// notify.
	const char *d_actor = object;
	json_t *user = ds_select_one(
		datastores[DS_USERS],
		CNDL(ISEQ_STR(actor, d_actor)));
	db_check_error(1);
	if (user != NULL) {
		json_decref(user);
		notify_object("Follow",
		              NULL,
		              json_object_get(activity, "object"),
		              json_object_get(activity, "actor"),
		              json_object_get(json_object_get(activity, "$$meta"),
		                              "inserted"));
	}

	json_decref(activity);
	json_decref(obj);
}

static char handle_Follow(transaction_t **trans, json_t *activity) {
	// check there isn't already an activity with the same actor and object.
	json_t *obj = ds_select_one(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(type, "Follow"),
		     ISEQ_STR(actor, json_object_string(activity, "actor")),
		     ISEQ_STR(object, json_object_string(activity, "object"))));
	db_check_error(1);
	if (obj != NULL) {
		json_decref(obj);
		return 0;
	}

	json_incref(activity);

	// fetch the destination actor.
	actor_get(json_object_string(activity, "object"), 1, _callback3, activity);

	return 1;
}

static char handle_Accept(transaction_t **trans, json_t *activity) {
	return 1;
}

static char handle_Reject(transaction_t **trans, json_t *activity) {
	return 1;
}

static char handle_Undo(transaction_t **trans, json_t *activity) {
	// get ID of pointed to activity.
	json_t *object = json_object_get(activity, "object");
	const char *id = NULL;
	if (json_is_string(object)) {
		id = json_string_value(object);
	} else {
		id = json_object_string(object, "id");
	}

	// get pointed activity and check it is owned by the actor.
	json_t *obj = ds_select_one(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(id, id),
		     ISEQ_STR(actor, json_object_string(activity, "actor"))));
	db_check_error(1);
	if (obj == NULL) {
		return 0;
	}

	if (strcmp(json_object_string(obj, "type"), "Follow") == 0) {
		// if it's a follow, delete the follow and its associated accept.
		const char *source = json_object_string(activity, "actor");
		const char *target = json_object_string(obj, "object");

		transaction_t *trans1 = trans_init(dbc);
		db_check_error(1);

		json_t *o;
		size_t i;

		json_t *follows1 = ds_select_one(
			datastores[DS_FOLLOWS],
			CNDL(ISEQ_STR(actor, target)));
		db_check_error(1);
		int j = -1;
		json_array_foreach(json_object_get(follows1, "followers"), i, o) {
			if (strcmp(json_string_value(o), source) == 0) {
				j = i;
				break;
			}
		}
		if (j != -1) {
			json_array_remove(json_object_get(follows1, "followers"), j);
			ds_update(datastores[DS_FOLLOWS], &trans1,
			          CNDL(ISEQ_STR(actor, target)), follows1);
		}

		json_t *follows2 = ds_select_one(
			datastores[DS_FOLLOWS],
			CNDL(ISEQ_STR(actor, source)));
		db_check_error(1);
		j = -1;
		json_array_foreach(json_object_get(follows2, "followings"), i, o) {
			if (strcmp(json_string_value(o), target) == 0) {
				j = i;
				break;
			}
		}
		if (j != -1) {
			json_array_remove(json_object_get(follows2, "followings"), j);
			ds_update(datastores[DS_FOLLOWS], &trans1,
			          CNDL(ISEQ_STR(actor, source)), follows2);
		}

		ds_delete(datastores[DS_ACTIVITIES], &trans1,
		          CNDL(ISEQ_STR(id, id)));
		db_check_error(1);
		ds_delete(datastores[DS_ACTIVITIES], &trans1,
		          CNDL(ISEQ_STR(type, "Accept"),
		               ISEQ_STR(object, id)));
		db_check_error(1);
		trans_commit(trans1);
		db_check_error(1);

		json_decref(follows1);
		json_decref(follows2);

		// notify.
		notify_object("Unfollow",
		              NULL,
		              json_object_get(obj, "object"),
		              json_object_get(activity, "actor"),
		              json_object_get(json_object_get(activity, "$$meta"),
		                              "inserted"));
	}

	json_decref(obj);

	return 1;
}

static char *activity_types[] = {
	"Create", (char*)handle_Create,
	"Like", (char*)handle_Like,
	"Announce", (char*)handle_Announce,
	"Follow", (char*)handle_Follow,
	"Accept", (char*)handle_Accept,
	"Reject", (char*)handle_Reject,
	"Undo", (char*)handle_Undo,
	NULL
};

char inbox_handle_activity(json_t *activity) {
	// validate the activity, initialize a transaction.
	transaction_t *trans = trans_init(dbc);
	db_check_error(1);

	ds_insert(datastores[DS_ACTIVITIES], &trans, activity);
	if (db_check_error(0)) {
		trans_rollback(trans);
		db_check_error(1);
		return 0;
	}

	// check it doesn't already exist.
	json_t *obj = ds_select_one(
		datastores[DS_ACTIVITIES],
		CNDL(ISEQ_STR(id, json_object_string(activity, "id"))));
	db_check_error(1);
	if (obj != NULL) {
		json_decref(obj);
		trans_rollback(trans);
		db_check_error(1);
		return 0;
	}

	const char *type = json_object_string(activity, "type");

	// check the type is known.
	char (*handler)(transaction_t**, json_t*) = NULL;
	for (int i = 0; activity_types[i] != NULL; i += 2) {
		if (strcmp(activity_types[i], type) == 0) {
			handler = (void*)activity_types[i + 1];
			break;
		}
	}

	if (handler == NULL) {
		syslog(LOG_WARNING, "%s%s%s",
		       i18n_gettext("smilodon.ap.activity.unhandled_a", NULL),
		       type,
		       i18n_gettext("smilodon.ap.activity.unhandled_b", NULL));
		trans_rollback(trans);
		db_check_error(1);
		return 1;
	}

	// pad inserted to 23 characters for correct ordering.
	char ibuf[24];
	snprintf(ibuf, 24, "%023" PRIi64, time(NULL));

	// add metadata.
	json_object_set_new(
		activity,
		"$$meta",
		json_pack("{soso}",
		          "id", json_sprintf("%" PRIu64, get_num_id()),
		          "inserted", json_string(ibuf)));

	// handle the activity.
	if (!handler(&trans, activity)) {
		trans_rollback(trans);
		db_check_error(1);
		return 0;
	}

	// commit to the database.
	trans_commit(trans);
	db_check_error(1);

	syslog(LOG_INFO, "%s%s",
	       i18n_gettext("smilodon.ap.activity.inserted", NULL),
	       json_object_string(activity, "id"));
	return 1;
}

void inbox_handle(FCGX_Request *req, HDF *hdf) {
	// try to parse the activity.
	json_t *activity = json_loads(
		hdf_get_value(hdf, "req.body.data", "null"),
		0, NULL);
	if (activity == NULL) {
		http_error(req, "400 Bad Request");
		return;
	}

	// get the actor ID and check it is valid.
	const char *actor_id = json_string_value(
		json_object_get(activity, "actor"));
	json_t *actor = actor_get_sync(actor_id, 1);
	if (actor == NULL) {
		json_decref(activity);
		http_error(req, "400 Bad Request");
		return;
	}
	json_decref(actor);

	// verify the signature.
	if (!http_verify(hdf, actor_id)) {
		json_decref(activity);
		http_error(req, "400 Bad Request");
		return;
	}

	// insert the activity.
	if (!inbox_handle_activity(activity)) {
		json_decref(activity);
		http_error(req, "400 Bad Request");
		return;
	}

	json_decref(activity);

	http_error(req, "200 OK");
}
