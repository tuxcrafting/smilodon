#include "smilodon/web/federation/federator.h"

#include <jansson.h>
#include <string.h>
#include <syslog.h>

#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/http_signature.h"
#include "smilodon/web/server.h"

static void _callback(char *s, void *data) {
	void **ctx = data;
	char **headers = ctx[0];
	sfree(s);
	sfree(headers[0]);
	sfree(headers[1]);
	sfree(headers);
	sfree(ctx[1]);
	sfree(ctx);
}

static void federate_to(json_t *activity, json_t *sent, const char *actor_id, json_t *inbox) {
	const char *inbox_s = json_string_value(inbox);
	json_array_append(sent, inbox);
	char *buf = json_dumps(activity, 0);
	syslog(LOG_INFO, "%s%s%s",
	       i18n_gettext("smilodon.ap.federation.federating_a", NULL),
	       inbox_s,
	       i18n_gettext("smilodon.ap.federation.federating_b", NULL));
	char **headers = http_sign(actor_id, inbox_s, buf);
	headers[2] = "Content-Type: " LD_JSON_AS;
	void **ctx = salloc(sizeof(void*) * 2);
	ctx[0] = headers;
	ctx[1] = buf;
	curlw_req(inbox_s, buf, (const char**)headers, _callback, ctx);
}

static void federate_actor(json_t *a, json_t *activity, json_t *sent) {
	const char *actor_id = json_object_string(activity, "actor");
	if (strcmp(json_object_string(
		           json_object_get(a, "$$meta"), "domain"),
	           server_name) != 0) {
		// send the activity to the remote inbox.
		json_t *inbox;
		json_t *endpoints = json_object_get(a, "endpoints");
		if (endpoints != NULL &&
		    json_object_get(endpoints, "sharedInbox") != NULL) {
			inbox = json_object_get(endpoints, "sharedInbox");
		} else {
			inbox = json_object_get(a, "inbox");
		}
		const char *inbox_s = json_string_value(inbox);
		json_t *obj1;
		size_t j;
		char found = 0;
		json_array_foreach(sent, j, obj1) {
			if (strcmp(json_string_value(obj1), inbox_s) == 0) {
				found = 1;
				break;
			}
		}
		if (!found) {
			federate_to(activity, sent, actor_id, inbox);
		}
	}
}

void federate(json_t *activity) {
	json_t *list = json_array();
	json_t *sent = json_array();
	json_array_extend(list, json_object_get(activity, "to"));
	json_array_extend(list, json_object_get(activity, "cc"));
	json_t *obj;
	size_t i;
	// federate it.
	json_array_foreach(list, i, obj) {
		json_t *a = ds_select_one(
			datastores[DS_ACTORS],
			CNDL(ISEQ_STR(id, json_string_value(obj))));
		db_check_error(1);
		if (a != NULL) {
			federate_actor(a, activity, sent);
			json_decref(a);
		} else {
			a = ds_select_one(
				datastores[DS_ACTORS],
				CNDL(ISEQ_STR(followers, json_string_value(obj))));
			db_check_error(1);
			if (a != NULL) {
				// send to all followers.
				json_t *follows = ds_select_one(
					datastores[DS_FOLLOWS],
					CNDL(ISEQ_STR(actor, json_object_string(a, "id"))));
				db_check_error(1);
				json_decref(a);
				if (follows != NULL) {
					json_t *o1;
					size_t i1;
					json_t *flist = json_object_get(follows, "followers");
					json_array_foreach(flist, i1, o1) {
						a = ds_select_one(
							datastores[DS_ACTORS],
							CNDL(ISEQ_STR(id, json_string_value(o1))));
						db_check_error(1);
						if (a != NULL) {
							federate_actor(a, activity, sent);
							json_decref(a);
						}
					}
					json_decref(follows);
				}
			}
		}
	}
	json_decref(list);
	json_decref(sent);
}
