/**
 * @file
 * @brief Webfinger requests.
 */

#pragma once

#include <jansson.h>

/**
 * @brief Do a Webfinger request.
 * @param username Username.
 * @param domain Domain.
 * @return Actor object or NULL in case of error.
 */
json_t *webfinger(const char *username, const char *domain);
