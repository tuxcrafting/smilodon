/**
 * @file
 * @brief ActivityPub object functions.
 */

#include <jansson.h>

/**
 * @brief Fetch an object and all its parents, if needed.
 * @param id AP ID.
 * @param callback Callback to be called with the object, or NULL in case of error.
 * @param callback_data Data to pass to the callback.
 */
void object_get(const char *id,
                void (*callback)(json_t*, void*), void *callback_data);
