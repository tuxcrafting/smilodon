#include "smilodon/activitypub/actor.h"

#include <jansson.h>
#include <string.h>

#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"

static json_t *_do(char *data, const char *id) {
	if (data == NULL) {
		return NULL;
	}

	json_t *obj = json_loads(data, 0, NULL);
	if (obj == NULL) {
		return NULL;
	}
	sfree(data);

	char buf[256] = { 0 };
	memccpy(buf, id + 8, '/', 255);
	*(char*)memchr(buf, '/', 256) = 0;

	json_object_set_new(
		obj, "$$meta",
		json_pack("{ss}", "domain", buf));

	transaction_t *trans = trans_init(dbc);
	db_check_error(1);

	ds_insert(datastores[DS_ACTORS], &trans, obj);
	if (db_check_error(0)) {
		json_decref(obj);
		trans_rollback(trans);
		return NULL;
	}

	json_t *o = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(id, json_object_string(obj, "id"))));
	db_check_error(1);
	if (o != NULL) {
		trans_rollback(trans);
		db_check_error(1);
		json_decref(o);
		return o;
	}

	trans_commit(trans);
	db_check_error(1);

	return obj;
}

static void _callback(char *data, void *_cdata) {
#define RETURN(v)	  \
	{ \
		void (*a)(json_t*, void*) = cdata[0]; \
		void *b = cdata[1]; \
		sfree(cdata); \
		a(v, b); \
		return; \
	}

	void **cdata = _cdata;
	char *id = cdata[2];

	RETURN(_do(data, id));
}

void actor_get(const char *id, char fetch,
               void (*callback)(json_t*, void*), void *callback_data) {
	json_t *obj = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(id, id)));
	if (obj == NULL && fetch) {
		void **cdata = salloc(sizeof(void*) * 3);
		cdata[0] = callback;
		cdata[1] = callback_data;
		cdata[2] = (char*)id;
		curlw_req(id, NULL, AP_ACCEPT, _callback, cdata);
	} else {
		callback(obj, callback_data);
	}
}

json_t *actor_get_sync(const char *id, char fetch) {
	json_t *obj = ds_select_one(
		datastores[DS_ACTORS],
		CNDL(ISEQ_STR(id, id)));
	if (obj == NULL && fetch) {
		obj = _do(curlw_req_sync(id, NULL, AP_ACCEPT), id);
	}
	return obj;
}
