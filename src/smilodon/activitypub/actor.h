/**
 * @file
 * @brief AP actor fetching functions.
 */

#pragma once

#include <jansson.h>

/**
 * @brief Get an actor.
 * @param id AP ID.
 * @param fetch If 1, will attempt to fetch it if there is no local copy.
 * @param callback Callback to be called with the actor, or NULL in case of error.
 * @param callback_data Data to pass to the callback.
 */
void actor_get(const char *id, char fetch,
               void (*callback)(json_t*, void*), void *callback_data);

/**
 * @brief Synchronously get an actor.
 * @param id AP ID.
 * @param fetch If 1, will attempt to fetch it if there is no local copy.
 * @return Actor object.
 */
json_t *actor_get_sync(const char *id, char fetch);
