#include "smilodon/activitypub/object.h"

#include <jansson.h>

#include "smilodon/activitypub/actor.h"
#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/db/database.h"
#include "smilodon/util.h"
#include "smilodon/web/federation/inbox.h"

#define RETURN(v)	  \
	{ \
		void (*a)(json_t*, void*) = cdata[0]; \
		void *b = cdata[1]; \
		sfree(cdata); \
		a(v, b); \
		return; \
	}

static void _callback3(json_t *obj, void *callback_data) {
	if (obj == NULL) {
		return;
	}
	json_t *p = json_object_get(obj, "inReplyTo");
	if (p != NULL && !json_is_null(p)) {
		const char *parent = json_string_value(p);
		object_get(parent, _callback3, NULL);
	}
	json_decref(obj);
}

static void _callback2(json_t *actor, void *_cdata) {
	void **cdata = _cdata;
	if (actor != NULL) {
		json_decref(actor);
	}
	json_t *obj = cdata[2];
	RETURN(obj);
}

static void _callback1(char *s, void *_cdata) {
	void **cdata = _cdata;

	json_t *obj = json_loads(s, 0, NULL);
	sfree(s);
	if (obj == NULL) {
		RETURN(NULL);
	}
	json_t *activity = json_pack(
		"{sOsssOsOsO}",
		"id", json_object_get(obj, "id"),
		"type", "Create",
		"actor", json_object_get(obj, "attributedTo"),
		"published", json_object_get(obj, "published"),
		"object", obj);
	inbox_handle_activity(activity);
	json_decref(activity);

	cdata[2] = obj;
	actor_get(json_object_string(obj, "attributedTo"), 1, _callback2, cdata);
}

void object_get(const char *id,
                void (*callback)(json_t*, void*), void *callback_data) {
	json_t *obj = ds_select_one(
		datastores[DS_OBJECTS],
		CNDL(ISEQ_STR(id, id)));
	if (obj == NULL) {
		void **cdata = salloc(sizeof(void*) * 3);
		cdata[0] = callback;
		cdata[1] = callback_data;
		curlw_req(id, NULL, AP_ACCEPT, _callback1, cdata);
		return;
	} else {
		json_incref(obj);
		_callback3(obj, NULL);
	}
	callback(obj, callback_data);
}
