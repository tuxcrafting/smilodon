#include "smilodon/curl.h"

#include <curl/curl.h>
#include <pthread.h>
#include <stddef.h>
#include <signal.h>
#include <syslog.h>
#include <time.h>

#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"

//#define DEBUG

#ifdef DEBUG
#define dbg(x) x
#else
#define dbg(x)
#endif

struct write_context {
	char *ptr;
	int len;
	int i;
};

static size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
	struct write_context *ctx = userdata;

	for (int i = 0; i < nmemb; i++) {
		if (ctx->i >= ctx->len) {
			ctx->ptr = srealloc(ctx->ptr, ctx->len += 1024);
		}
		ctx->ptr[ctx->i++] = ptr[i];
	}

	if (ctx->i >= ctx->len) {
		ctx->ptr = srealloc(ctx->ptr, ctx->len += 1024);
	}
	ctx->ptr[ctx->i] = 0;

	return nmemb;
}

#ifdef DEBUG
static void debug(CURL *curl, curl_infotype type, char *data, size_t size, void *ctx) {
	switch (type) {
	case CURLINFO_TEXT:
		printf("CURL TEXT\n");
		break;
	case CURLINFO_HEADER_IN:
		printf("CURL HEADER IN\n");
		break;
	case CURLINFO_HEADER_OUT:
		printf("CURL HEADER OUT\n");
		break;
	case CURLINFO_DATA_IN:
		printf("CURL DATA IN\n");
		break;
	case CURLINFO_DATA_OUT:
		printf("CURL DATA OUT\n");
		break;
	default:
		return;
	}
	printf("%.*s\n", (int)size, data);
}
#endif

static char *_curlw_do_req(const char *url, const char *postdata, const char **headers,
                           pthread_mutex_t *mutex, pthread_cond_t *cond) {
	CURL *curl = curl_easy_init();
	if (curl == NULL) {
		return NULL;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);

	if (postdata != NULL) {
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);
	}

	struct curl_slist *slist = NULL;
	if (headers != NULL) {
		for (int i = 0; headers[i] != NULL; i++) {
			slist = curl_slist_append(slist, headers[i]);
		}
	}

	struct write_context ctx = { 0 };
	ctx.ptr = salloc(1);
	ctx.len = 1;

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ctx);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	dbg(curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L));
	dbg(curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, debug));

	if (postdata == NULL) {
		syslog(LOG_INFO, "%s%s",
		       i18n_gettext("smilodon.curl.fetching", NULL), url);
	}

	if (cond != NULL) {
		pthread_mutex_lock(mutex);
		pthread_cond_signal(cond);
		pthread_mutex_unlock(mutex);
	}

	CURLcode res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		sfree(ctx.ptr);
		ctx.ptr = NULL;
		syslog(LOG_ERR, "%s%s",
		       i18n_gettext("smilodon.curl.error", NULL),
		       curl_easy_strerror(res));
	}

	curl_easy_cleanup(curl);
	curl_slist_free_all(slist);

	return ctx.ptr;
}

void *_thread_main(void *data) {
	void **ctx = data;
	const char *url = ctx[0];
	const char *postdata = ctx[1];
	const char **headers = ctx[2];
	void (*callback)(char*, void*) = ctx[3];
	void *callback_data = ctx[4];
	pthread_mutex_t *mutex = ctx[5];
	pthread_cond_t *cond = ctx[6];
	void *a = _curlw_do_req(url, postdata, headers, mutex, cond);
	enqueue_callback((void(*)(void*, void*))callback, a, callback_data, pthread_self());
	return NULL;
}

void curlw_req(const char *url, const char *postdata, const char **headers,
               void (*callback)(char*, void*), void *callback_data) {
	// initialize pthread variables.
	pthread_cond_t cond;
	pthread_mutex_t mutex;
	pthread_cond_init(&cond, NULL);
	pthread_mutex_init(&mutex, NULL);

	// create a thread.
	pthread_t thread;
	const void *ctx[7] = {
		url, postdata, headers,
		callback, callback_data,
		&mutex, &cond,
	};
	pthread_mutex_lock(&mutex);
	pthread_create(&thread, NULL, _thread_main, ctx);

	// wait until the transfer has started and return to the main thread.
	pthread_cond_wait(&cond, &mutex);
	pthread_mutex_unlock(&mutex);
	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&mutex);
}

char *curlw_req_sync(const char *url, const char *postdata, const char **headers) {
	return _curlw_do_req(url, postdata, headers, NULL, NULL);
}
