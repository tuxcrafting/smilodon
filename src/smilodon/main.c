#include <curl/curl.h>
#include <jansson.h>
#include <libxml/xmlmemory.h>
#include <openssl/crypto.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <time.h>
#include <util/neo_err.h>

#include "smilodon/config.h"
#include "smilodon/curl.h"
#include "smilodon/database.h"
#include "smilodon/datadir.h"
#include "smilodon/i18n.h"
#include "smilodon/util.h"
#include "smilodon/web/server.h"

static char conf_loaded;

static void *_zalloc(size_t n) { return OPENSSL_zalloc(n); }
static void _free(void *p) { OPENSSL_free(p); }
static void *_realloc(void *p, size_t n) { return OPENSSL_realloc(p, n); }
static char *_strdup(const char *s) { return OPENSSL_strdup(s); }

static void sigint(int sig) {
	stop_server();
}

static void unmain() {
	stop_server();
	database_close();
	curl_global_cleanup();
	if (conf_loaded) {
		syslog(LOG_INFO, "%s", i18n_gettext("smilodon.bye", NULL));
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <configuration file.hdf>\n", argv[0]);
		return 1;
	}

	srand(time(NULL));

	// set atexit and signal handlers.
	atexit(unmain);

	struct sigaction act = { 0 };
	act.sa_handler = sigint;
	sigaction(SIGINT, &act, NULL);

	signal(SIGPIPE, SIG_IGN);

	// set the allocation functions to openssl's.
	OPENSSL_malloc_init();
	alloc_fn = _zalloc;
	free_fn = _free;
	realloc_fn = _realloc;
	strdup_fn = _strdup;
	json_set_alloc_funcs(salloc, sfree);
	xmlMemSetup(sfree, salloc, srealloc, sstrdup);

	// seed jansson.
	json_object_seed(0);

	nerr_init();
	curl_global_init(CURL_GLOBAL_DEFAULT);

	openlog("smilodon", LOG_PERROR | LOG_PID, LOG_USER);

	config_load(argv[1]);
	conf_loaded = 1;

	datadir_init();
	i18n_init();

	syslog(LOG_INFO, "%s", i18n_gettext("smilodon.welcome", NULL));

	database_open();

	start_server(config_get("server.fcgi_addr", (char*)-1),
	             atoi(config_get("server.workers", "8")),
	             atoi(config_get("server.backlog", "100")));

	return 0;
}
