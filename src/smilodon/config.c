#include "smilodon/config.h"

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <util/neo_hdf.h>

#include "smilodon/util.h"

static HDF *config_hdf;

const char *secret;

void config_load(const char *filename) {
	FILE *f = fopen(filename, "r");
	if (f == NULL) {
		syslog(LOG_ERR, "Couldn't open %s.", filename);
		exit(1);
	}
	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);

	char *buf = salloc(len + 1);
	fread(buf, 1, len, f);

	fclose(f);

	check_nerr(hdf_init(&config_hdf));
	check_nerr(hdf_read_string(config_hdf, buf));

	sfree(buf);

	secret = config_get("secret", NULL);
}

const char *config_get(const char *name, const char *def) {
	char *s = hdf_get_value(config_hdf, name, def);
	if (s == (char*)-1) {
		syslog(LOG_ERR, "Required configuration entry %s doesn't exist.", name);
		exit(1);
	}
	return s;
}
