/**
 * @file
 * @brief CURL wrappers.
 */

#pragma once

#include <signal.h>

/// @brief Content type used for most ActivityPub things.
#define LD_JSON_AS "application/activity+json"
/// @brief Header list to fetch an AP object.
#define AP_ACCEPT (const char*[]){ "Accept: " LD_JSON_AS, NULL }

/**
 * @brief Do an HTTP request.
 * @param url Request URL.
 * @param postdata Post data, or NULL for a simple GET.
 * @param headers NULL-terminated list of headers.
 * @param callback Callback to be called with the fetched data, or NULL in case of error.
 * @param callback_data User data to be passed to the callback.
 */
void curlw_req(const char *url, const char *postdata, const char **headers,
               void (*callback)(char*, void*), void *callback_data);

/**
 * @brief Do a synchronous HTTP request.
 * @param url Request URL.
 * @param postdata Post data, or NULL for a simple GET.
 * @param headers NULL-terminated list of headers.
 * @return Fetched data, or NULL in case of error.
 */
char *curlw_req_sync(const char *url, const char *postdata, const char **headers);
