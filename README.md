# Smilodon

**Important: This is abandoned but I'll probably release a successor. At some unspecified point in the future. Stay tuned!**

*NOTE: This project is unrelated to [the Python AP server](https://github.com/rowanlupton/smilodon).*

![smilodon "logo"](smilodon.png)

Smilodon is a federated microblogging platform.

## Building

First, ensure you have its dependencies:

* A POSIX-compatible operating system
* PostgreSQL
* A FastCGI-capable web server
* A syslog daemon
* Argon2
* OpenSSL/LibreSSL
* FastCGI SDK
* Jansson
* ClearSilver
* LibCURL
* LibPQ
* LibXML2

Then, clone the repository:

    $ git clone --recurse-submodules https://gitlab.com/tuxcrafting/smilodon.git
    $ cd smilodon

Copy the example configuration and create the build directory:

    $ mkdir build
    $ cd build
    $ cp ../example.hdf ./config.hdf

Edit the configuration file. The options that you'll probably need to change
are `server_name`, `secret` and `database.connection_string`.

Then, create its PostgreSQL user and database:

    postgres=# CREATE USER <user> PASSWORD '<password>';
    postgres=# CREATE DATABASE <database> OWNER <user>;

Configure your web server to proxy through FastCGI to the address in the configuration,
and finally, build it and run it:

    $ cd ..
    $ ./make
    $ ./build/smilodon ./build/config.hdf
